import React from "react";
import PageHeader from "../../components/PageHeader";

const PageNotFound = () => {
  return (
    <section className="container">
      <PageHeader pageTitle={"Page Not Found"} />
    </section>
  );
};

export default PageNotFound;
