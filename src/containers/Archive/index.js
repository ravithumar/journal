import React, { useEffect, useState } from "react";
import { Translation } from "react-i18next";
import { Link } from "react-router-dom";
import Loader from "../../components/Loader";
import PageHeader from "../../components/PageHeader";
import { getApi } from "../../utils/ServiceManager";
import styles from "./archive.module.scss";

const Archive = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    getData();
  }, []);
  const getData = () => {
    setIsLoading(true);
    getApi("issues/list", onSuccess, onFailure);
  };
  const onSuccess = (res) => {
    setIsLoading(false);
    if (res?.status) {
      setData(res?.data?.issues);
    } else {
      setError(res?.message);
    }
  };
  const onFailure = (err) => {
    setIsLoading(false);
    // setError(JSON.stringify(err?.message));
  };
  return (
    <Translation>
      {(t) => (
        <section className="container">
          <PageHeader pageTitle={t("Header.archive")} />
          <div className="d-flex flex-wrap justify-content-between gap-2">
            {isLoading ? (
              <Loader />
            ) : (
              <>
                {data.length !== 0 &&
                  data?.map((x) => {
                    return (
                      <Link
                        key={x?.id}
                        state={{ id: x?.id }}
                        to={`/archive/${x?.name}`}
                        className={`col-12 col-md-5 col-lg-3 d-flex flex-column justify-content-center align-items-center my-4 py-4 rounded-3 ${styles.box}`}
                      >
                        <h2 className={styles.year}>{x?.year}</h2>
                        <div
                          className={`d-flex justify-content-center w-75 ${styles.boxDesc}`}
                        >
                          <p className="fs-4 m-0">{x?.name}</p>
                        </div>
                      </Link>
                    );
                  })}
              </>
            )}
          </div>
          {/* {error && <h2 className="text-danger text-center">{error}</h2>} */}
        </section>
      )}
    </Translation>
  );
};

export default Archive;
