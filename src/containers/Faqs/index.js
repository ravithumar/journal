import React, { useContext, useEffect, useState } from "react";
import {
  Accordion,
  AccordionContext,
  Card,
  useAccordionButton,
} from "react-bootstrap";
import PageHeader from "../../components/PageHeader";
import plusIcon from "../../assets/images/plusIcon.png";
import minusIcon from "../../assets/images/minusIcon.png";
import styles from "./faqs.module.scss";
import { getApi } from "../../utils/ServiceManager";
import { Translation } from "react-i18next";
import Loader from "../../components/Loader";

const Faqs = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    getData();
  }, []);
  const getData = () => {
    setIsLoading(true);
    getApi("faqs", onSuccess, onFailure);
  };
  const onSuccess = (res) => {
    setIsLoading(false);
    if (res?.status) {
      setData(res?.data?.faqs);
    } else {
      setError(res?.message);
    }
  };
  const onFailure = (err) => {
    setIsLoading(false);
    // setError(JSON.stringify(err?.message));
  };
  const ContextAwareToggle = ({ children, eventKey, callback }) => {
    const { activeEventKey } = useContext(AccordionContext);
    const decoratedOnClick = useAccordionButton(
      eventKey,
      () => callback && callback(eventKey)
    );
    const isCurrentEventKey = activeEventKey === eventKey;
    return (
      <button
        type="button"
        onClick={decoratedOnClick}
        className="border-0 bg-transparent d-flex align-items-center gap-1 gap-md-3"
      >
        {isCurrentEventKey ? (
          <div>
            <img style={{ height: "1rem" }} src={minusIcon} alt="Separator" />
          </div>
        ) : (
          <div>
            <img style={{ height: "1rem" }} src={plusIcon} alt="Separator" />
          </div>
        )}
        {children}
      </button>
    );
  };

  return (
    <Translation>
      {(t) => (
        <section className="container mb-5">
          <PageHeader pageTitle={t("Header.FAQsFull")} />
          <Accordion className="gap-4 d-flex flex-column mt-4">
            {isLoading ? (
              <Loader />
            ) : (
              <>
                {data.length !== 0 &&
                  data?.map((x, index) => {
                    return (
                      <Card
                        style={{ zIndex: 20 }}
                        key={index}
                        className="bg-white bg-opacity-10 rounded-3"
                      >
                        <Card.Header className="d-flex m-4 p-0 border-0 bg-transparent">
                          <ContextAwareToggle eventKey={index}>
                            <h2
                              className={`h4 fw-bold mb-0 ${styles.titleFaq}`}
                            >
                              {x?.que}
                            </h2>
                          </ContextAwareToggle>
                        </Card.Header>
                        <Accordion.Collapse eventKey={index}>
                          <Card.Body className={`p-0 my-3 ${styles.bodyFaq}`}>
                            <ul className="list-unstyled m-0 ms-3">
                              <li>{x?.ans}</li>
                            </ul>
                          </Card.Body>
                        </Accordion.Collapse>
                      </Card>
                    );
                  })}
              </>
            )}
          </Accordion>
          {/* {error && <h2 className="text-danger text-center">{error}</h2>} */}
        </section>
      )}
    </Translation>
  );
};

export default Faqs;
