import React, { useEffect, useState } from "react";
import styles from "./homeArticle.module.scss";
import article1 from "../../assets/images/article1.png";
import arrowForward from "../../assets/images/arrow-forward.png";
import { Link } from "react-router-dom";
import i18next from "i18next";
import { getApi } from "../../utils/ServiceManager";
import { Tab, Tabs } from "react-bootstrap";
import Loader from "../../components/Loader";
import starBg from "../../assets/images/starBg.png";
import articlesTitle from "../../assets/images/articlesTitle.png";
import articlesTitleAr from "../../assets/images/articlesTitleAr.png";

const HomeArticle = () => {
  const [data, setData] = useState([]);
  const [read, setRead] = useState([]);
  const [download, setDownload] = useState([]);
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [isReadLoading, setIsReadLoading] = useState(false);
  const [isdownloadLoading, setIsDownloadLoading] = useState(false);
  useEffect(() => {
    getData();
    getRead();
    getDownload();
  }, []);
  const getData = () => {
    setIsLoading(true);
    getApi("issues/home?type=latest", onSuccess, onFailure);
  };
  const onSuccess = (res) => {
    setIsLoading(false);
    if (res?.status) {
      setData(res?.data?.articles);
    } else {
      setError(res?.message);
    }
  };
  const onFailure = (err) => {
    setIsLoading(false);
    // setError(JSON.stringify(err?.message));
  };
  const getRead = () => {
    setIsReadLoading(true);
    getApi("issues/home?type=most-read", onSuccessRead, onFailureRead);
  };
  const onSuccessRead = (res) => {
    setIsReadLoading(false);
    if (res?.status) {
      setRead(res?.data?.articles);
    } else {
      setError(res?.message);
    }
  };
  const onFailureRead = (err) => {
    setIsReadLoading(false);
    // setError(JSON.stringify(err?.message));
  };
  const getDownload = () => {
    setIsDownloadLoading(true);
    getApi(
      "issues/home?type=most-downloaded",
      onSuccessDownload,
      onFailureDownload
    );
  };
  const onSuccessDownload = (res) => {
    setIsDownloadLoading(false);
    if (res?.status) {
      setDownload(res?.data?.articles);
    } else {
      setError(res?.message);
    }
  };
  const onFailureDownload = (err) => {
    setIsDownloadLoading(false);
    // setError(JSON.stringify(err?.message));
  };
  return (
    <>
      {/* <img
        className={`d-block w-50 position-absolute ${
          i18next.language === "ar" ? styles.imgAr : styles.img
        }`}
        src={starBg}
        alt="First slide"
      />

      <section className="container my-5 position-relative">
        <img
          className={`position-absolute ${
            i18next.language === "ar"
              ? styles.articlesTitleImgAr
              : styles.articlesTitleImg
          }`}
          src={i18next.language === "ar" ? articlesTitleAr : articlesTitle}
          alt="First slide"
        />
        <div className="d-flex flex-column flex-xxl-row bg-white bg-opacity-10 rounded-3">
          <div className="col-xxl-8 px-3 pt-3 px-md-4 pt-md-4 px-xl-5 pt-xl-5 pb-2 d-flex flex-column flex-md-row gap-3 gap-xxl-4">
            <div className="col-md-7">
              <h2 className={`mb-5 ${styles.title}`}>
                {i18next.language === "ar"
                  ? "أبحاث العدد الحالي"
                  : "Current Issues"}
              </h2>
              <img
                className={`d-block ${styles.articleCover}`}
                src={
                  data[0]?.issue_cover_img ? data[0]?.issue_cover_img : article1
                }
                alt=""
              />
            </div>
            <div className="col-md-5 mt-md-5 pt-md-5 gap-lg-2 mb-auto gap-xxl-1 d-flex flex-column">
              {isLoading ? (
                <Loader />
              ) : (
                <>
                  {data?.length !== 0 &&
                    data?.map((x) => {
                      return (
                        <Link
                          to={`/archive/${x?.issue}/${x?.id}/${x?.title
                            ?.toLowerCase()
                            .replace(/ /g, "-")}`}
                          key={x?.id}
                        >
                          <h5
                            className={`h5 bold text-white-50 ${
                              i18next.language === "ar"
                                ? styles.articleTitleAr
                                : styles.articleTitle
                            }`}
                          >
                            {x?.title}
                          </h5>
                          <p className={`fs-6 ${styles.articleDesc}`}>
                            {x?.author}
                          </p>
                        </Link>
                      );
                    })}
                </>
              )}
              <Link
                to={`/archive/${data[0]?.issue}`}
                state={{ id: data[0]?.issue_id }}
                className="d-flex gap-2 align-items-center"
              >
                <p className="fs-5 text-white mb-0">
                  {i18next.language === "ar" ? "مشاهدة الكل" : "View All"}
                </p>
                <div className="d-flex">
                  <div className="w-auto mb-1">
                    <img className="d-block w-100" src={arrowForward} alt="" />
                  </div>
                  <div className="w-auto">
                    <img className="d-block w-100" src={arrowForward} alt="" />
                  </div>
                </div>
              </Link>
            </div>
          </div>
          <div
            className={`col-xxl-4 p-3 p-md-4 pb-2 pe-xxl-5 ps-xxl-1 pt-xxl-5 ${styles.categoryArticle}`}
          >
            <Tabs
              style={{ width: "fit-content" }}
              defaultActiveKey="read"
              direction={i18next.language === "ar" ? "rtl" : "ltr"}
              className={`${styles.tabs}`}
              id="noanim-tab-example"
            >
              <Tab
                eventKey="read"
                tabClassName={`text-white ${
                  i18next.language === "ar"
                    ? styles.categoryArticleTitleAr
                    : styles.categoryArticleTitle
                }`}
                title={
                  i18next.language === "ar"
                    ? "الأبحاث الأكثر قراءة"
                    : "Most Read Articles"
                }
              >
                {isReadLoading ? (
                  <Loader />
                ) : (
                  read?.length !== 0 &&
                  read?.map((x) => {
                    return (
                      <Link
                        to={`/archive/${x?.issue}/${x?.id}/${x?.title
                          ?.toLowerCase()
                          .replace(/ /g, "-")}`}
                        key={x?.id}
                      >
                        <h5
                          className={`h5 bold text-white-50 ${
                            i18next.language === "ar"
                              ? styles.articleTitleAr
                              : styles.articleTitle
                          }`}
                        >
                          {x?.title}
                        </h5>
                        <p className={`fs-6 ${styles.articleDesc}`}>
                          {x?.author}
                        </p>
                      </Link>
                    );
                  })
                )}
              </Tab>
              <Tab
                eventKey="download"
                tabClassName={`text-white ${
                  i18next.language === "ar"
                    ? styles.categoryArticleTitleAr
                    : styles.categoryArticleTitle
                }`}
                title={
                  i18next.language === "ar"
                    ? "الأبحاث الأكثر تنزيلاً"
                    : "Most Download Articles"
                }
              >
                {isdownloadLoading ? (
                  <Loader />
                ) : (
                  download !== [] &&
                  download?.map((x, index) => {
                    return (
                      <Link
                        to={`/archive/${x?.issue}/${x?.id}/${x?.title
                          ?.toLowerCase()
                          .replace(/ /g, "-")}`}
                        key={x?.id}
                      >
                        <h5
                          className={`h5 bold text-white-50 ${
                            i18next.language === "ar"
                              ? styles.articleTitleAr
                              : styles.articleTitle
                          }`}
                        >
                          {x?.title}
                        </h5>
                        <p className={`fs-6 ${styles.articleDesc}`}>
                          {x?.author}
                        </p>
                      </Link>
                    );
                  })
                )}
              </Tab>
            </Tabs>
            <Link to={"/archive"} className="d-flex gap-2 align-items-center">
              <p className="fs-5 text-white mb-0">
                {i18next.language === "ar" ? "مشاهدة الكل" : "View All"}
              </p>
              <div className="d-flex">
                <div className="w-auto mb-1">
                  <img className="d-block w-100" src={arrowForward} alt="" />
                </div>
                <div className="w-auto">
                  <img className="d-block w-100" src={arrowForward} alt="" />
                </div>
              </div>
            </Link>
          </div>
        </div>
      </section> */}










      <div className={`${styles.headBackground}`}>
        <section className="container py-5">
        <h4 className={`text-center mb-5 pb-1 ${styles.articleeTitle}`}>
            {i18next.language === "ar" ? "مقالات " : "Article"}
          </h4>
          <div className="row">
            <div className="col-md-6">
              <div className="d-flex flex-row">
                <div>
                  <img
                    className={`${styles.articleCover}`}
                    src={
                      data[0]?.issue_cover_img ? data[0]?.issue_cover_img : article1
                    }
                    alt=""
                  />
                </div>

                <div className="mx-3">
                  {isLoading ? (
                    <Loader />
                  ) : (
                    <>

                      <p className={`${styles.title}`}>
                        {i18next.language === "ar"
                          ? "أبحاث العدد الحالي"
                          : "Current Issues"}
                      </p>

                      {data?.length !== 0 &&
                        data?.map((x) => {
                          return (
                            <div className={`mt-3 ${styles.articleDiv}`}>
                              <Link
                                to={`/archive/${x?.issue}/${x?.id}/${x?.title
                                  ?.toLowerCase()
                                  .replace(/ /g, "-")}`}
                                key={x?.id}
                              >
                                <h5
                                  className={`h5 bold m-0 ${i18next.language === "ar"
                                    ? styles.articleTitleAr
                                    : styles.articleTitle
                                    }`}
                                >
                                  {x?.title}
                                </h5>
                                <p className={`m-0 fs-6 ${styles.articleDesc}`}>
                                  {x?.author}
                                </p>
                              </Link>
                            </div>

                          );
                        })}
                    </>
                  )}
                  <Link
                    to={`/archive/${data[0]?.issue}`}
                    state={{ id: data[0]?.issue_id }}
                   
                  >
                    <p  className={`${styles.viewAllArticle}`}>
                      {i18next.language === "ar" ? "مشاهدة الكل" : "View All"}
                    </p>
                    {/* <div className="d-flex">
                      <div className="w-auto mb-1">
                        <img className="d-block w-100" src={arrowForward} alt="" />
                      </div>
                      <div className="w-auto">
                        <img className="d-block w-100" src={arrowForward} alt="" />
                      </div>
                    </div> */}
                  </Link>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <Tabs
                style={{ width: "fit-content" }}
                defaultActiveKey="read"
                direction={i18next.language === "ar" ? "rtl" : "ltr"}
                className={`${styles.tabs}`}
                id="noanim-tab-example"
              >
                <Tab
                  eventKey="read"
                  tabClassName={`${i18next.language === "ar"
                    ? styles.categoryArticleTitleAr
                    : styles.categoryArticleTitle
                    }`}
                  title={
                    i18next.language === "ar"
                      ? "الأبحاث الأكثر قراءة"
                      : "Most Read Articles"
                  }
                >
                  {isReadLoading ? (
                    <Loader />
                  ) : (
                    read?.length !== 0 &&
                    read?.map((x) => {
                      return (
                        <div className={`mt-3 ${styles.articleDiv}`}>
                          <Link
                            to={`/archive/${x?.issue}/${x?.id}/${x?.title
                              ?.toLowerCase()
                              .replace(/ /g, "-")}`}
                            key={x?.id}
                          >
                            <h5
                              className={`h5 bold m-0 ${i18next.language === "ar"
                                ? styles.articleTitleAr
                                : styles.articleTitle
                                }`}
                            >
                              {x?.title}
                            </h5>
                            <p className={`m-0 fs-6 ${styles.articleDesc}`}>
                              {x?.author}
                            </p>
                          </Link>
                        </div>
                      );
                    })
                  )}

                </Tab>
                <Tab
                  eventKey="download"
                  tabClassName={`${i18next.language === "ar"
                    ? styles.categoryArticleTitleAr
                    : styles.categoryArticleTitle
                    }`}
                  title={
                    i18next.language === "ar"
                      ? "الأبحاث الأكثر تنزيلاً"
                      : "Most Download Articles"
                  }
                >
                  {isdownloadLoading ? (
                    <Loader />
                  ) : (
                    download !== [] &&
                    download?.map((x, index) => {
                      return (
                        <div className={`mt-3 ${styles.articleDiv}`}>
                          <Link
                            to={`/archive/${x?.issue}/${x?.id}/${x?.title
                              ?.toLowerCase()
                              .replace(/ /g, "-")}`}
                            key={x?.id}
                          >
                            <h5
                              className={`h5 bold m-0 ${i18next.language === "ar"
                                ? styles.articleTitleAr
                                : styles.articleTitle
                                }`}
                            >
                              {x?.title}
                            </h5>
                            <p className={`m-0 fs-6 ${styles.articleDesc}`}>
                              {x?.author}
                            </p>
                          </Link>
                        </div>
                      );
                    })
                  )}
                </Tab>
              </Tabs>
              <Link to={"/archive"} className="">
                <p className={`${styles.viewAllArticle}`}>
                  {i18next.language === "ar" ? "مشاهدة الكل" : "View All"}
                </p>
                {/* <div className="d-flex">
                  <div className="w-auto mb-1">
                    <img className="d-block w-100" src={arrowForward} alt="" />
                  </div>
                  <div className="w-auto">
                    <img className="d-block w-100" src={arrowForward} alt="" />
                  </div>
                </div> */}
              </Link>
            </div>
          </div>



















          {/* <div className="d-flex flex-column flex-xxl-row bg-white bg-opacity-10 rounded-3">
            <div className="col-xxl-8 px-3 pt-3 px-md-4 pt-md-4 px-xl-5 pt-xl-5 pb-2 d-flex flex-column flex-md-row gap-3 gap-xxl-4">
              <div className="col-md-7">
                <h2 className={`mb-5 ${styles.title}`}>
                  {i18next.language === "ar"
                    ? "أبحاث العدد الحالي"
                    : "Current Issues"}
                </h2>
                <img
                  className={`d-block ${styles.articleCover}`}
                  src={
                    data[0]?.issue_cover_img ? data[0]?.issue_cover_img : article1
                  }
                  alt=""
                />
              </div>
              <div className="col-md-5 mt-md-5 pt-md-5 gap-lg-2 mb-auto gap-xxl-1 d-flex flex-column">
                {isLoading ? (
                  <Loader />
                ) : (
                  <>
                    {data?.length !== 0 &&
                      data?.map((x) => {
                        return (
                          <Link
                            to={`/archive/${x?.issue}/${x?.id}/${x?.title
                              ?.toLowerCase()
                              .replace(/ /g, "-")}`}
                            key={x?.id}
                          >
                            <h5
                              className={`h5 bold ${i18next.language === "ar"
                                ? styles.articleTitleAr
                                : styles.articleTitle
                                }`}
                            >
                              {x?.title}
                            </h5>
                            <p className={`fs-6 ${styles.articleDesc}`}>
                              {x?.author}
                            </p>
                          </Link>
                        );
                      })}
                  </>
                )}
                <Link
                  to={`/archive/${data[0]?.issue}`}
                  state={{ id: data[0]?.issue_id }}
                  className="d-flex gap-2 align-items-center"
                >
                  <p className="fs-5 text-white mb-0">
                    {i18next.language === "ar" ? "مشاهدة الكل" : "View All"}
                  </p>
                  <div className="d-flex">
                    <div className="w-auto mb-1">
                      <img className="d-block w-100" src={arrowForward} alt="" />
                    </div>
                    <div className="w-auto">
                      <img className="d-block w-100" src={arrowForward} alt="" />
                    </div>
                  </div>
                </Link>
              </div>
            </div>

            <div
              className={`col-xxl-4 p-3 p-md-4 pb-2 pe-xxl-5 ps-xxl-1 pt-xxl-5 ${styles.categoryArticle}`}
            >
              <Tabs
                style={{ width: "fit-content" }}
                defaultActiveKey="read"
                direction={i18next.language === "ar" ? "rtl" : "ltr"}
                className={`${styles.tabs}`}
                id="noanim-tab-example"
              >
                <Tab
                  eventKey="read"
                  tabClassName={`text-white ${i18next.language === "ar"
                    ? styles.categoryArticleTitleAr
                    : styles.categoryArticleTitle
                    }`}
                  title={
                    i18next.language === "ar"
                      ? "الأبحاث الأكثر قراءة"
                      : "Most Read Articles"
                  }
                >
                  {isReadLoading ? (
                    <Loader />
                  ) : (
                    read?.length !== 0 &&
                    read?.map((x) => {
                      return (
                        <Link
                          to={`/archive/${x?.issue}/${x?.id}/${x?.title
                            ?.toLowerCase()
                            .replace(/ /g, "-")}`}
                          key={x?.id}
                        >
                          <h5
                            className={`h5 bold ${i18next.language === "ar"
                              ? styles.articleTitleAr
                              : styles.articleTitle
                              }`}
                          >
                            {x?.title}
                          </h5>
                          <p className={`fs-6 ${styles.articleDesc}`}>
                            {x?.author}
                          </p>
                        </Link>
                      );
                    })
                  )}
                </Tab>
                <Tab
                  eventKey="download"
                  tabClassName={`text-white ${i18next.language === "ar"
                    ? styles.categoryArticleTitleAr
                    : styles.categoryArticleTitle
                    }`}
                  title={
                    i18next.language === "ar"
                      ? "الأبحاث الأكثر تنزيلاً"
                      : "Most Download Articles"
                  }
                >
                  {isdownloadLoading ? (
                    <Loader />
                  ) : (
                    download !== [] &&
                    download?.map((x, index) => {
                      return (
                        <Link
                          to={`/archive/${x?.issue}/${x?.id}/${x?.title
                            ?.toLowerCase()
                            .replace(/ /g, "-")}`}
                          key={x?.id}
                        >
                          <h5
                            className={`h5 bold ${i18next.language === "ar"
                              ? styles.articleTitleAr
                              : styles.articleTitle
                              }`}
                          >
                            {x?.title}
                          </h5>
                          <p className={`fs-6 ${styles.articleDesc}`}>
                            {x?.author}
                          </p>
                        </Link>
                      );
                    })
                  )}
                </Tab>
              </Tabs>
              <Link to={"/archive"} className="d-flex gap-2 align-items-center">
                <p className="fs-5 text-white mb-0">
                  {i18next.language === "ar" ? "مشاهدة الكل" : "View All"}
                </p>
                <div className="d-flex">
                  <div className="w-auto mb-1">
                    <img className="d-block w-100" src={arrowForward} alt="" />
                  </div>
                  <div className="w-auto">
                    <img className="d-block w-100" src={arrowForward} alt="" />
                  </div>
                </div>
              </Link>
            </div>
          </div> */}
        </section>

      </div>
    </>
  );
};

export default HomeArticle;
