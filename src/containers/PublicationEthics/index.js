import React, { useEffect, useState } from "react";
import { Translation } from "react-i18next";
import Loader from "../../components/Loader";
import PageHeader from "../../components/PageHeader";
import useMarkdown from "../../hooks/useMarkdown";
import { getApi } from "../../utils/ServiceManager";
import styles from "./publicationEthics.module.scss";

const PublicationEthics = () => {
  const [data, setData] = useState("");
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    getData();
  }, []);
  const getData = () => {
    setIsLoading(true);
    getApi("publication-ethics", onSuccess, onFailure);
  };
  const onSuccess = (res) => {
    setIsLoading(false);
    if (res?.status) {
      setData(res?.data?.content);
    } else {
      setError(res?.message);
    }
  };
  const onFailure = (err) => {
    setIsLoading(false);
    // setError(JSON.stringify(err?.message));
  };
  const { HTML } = useMarkdown(data);

  return (
    <Translation>
      {(t) => (
        <section className="container mb-5">
          <PageHeader pageTitle={t("Header.publicationEthics")} />
          <div className="d-flex flex-column my-4 py-3 py-lg-4 px-3 rounded-3 bg-opacity-10 bg-white">
            {/* {error && <h2>{error}</h2>} */}
            {isLoading ? (
              <Loader />
            ) : (
              data !== "" && (
                <div
                  className={styles.pEthicsHtml}
                  dangerouslySetInnerHTML={{
                    __html: HTML,
                  }}
                ></div>
              )
            )}
            {/* {error && <h2 className="text-danger text-center">{error}</h2>} */}
          </div>
        </section>
      )}
    </Translation>
  );
};

export default PublicationEthics;
