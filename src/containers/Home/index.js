import React from "react";
import separator from "../../assets/images/separator.png";
import HomeSearch from "../HomeSearch";
import LatestNews from "../LatestNews";
import HomeBanner from "../HomeBanner";
import HomeAbout from "../HomeAbout";
import HomeArticle from "../HomeArticle";

const Home = (props) => {
  return (
    <>
      <HomeBanner />
      {/* <img
        className="d-block w-100 container"
        style={{ height: "1rem" }}
        src={separator}
        alt="Separator"
      /> */}
      <HomeAbout />
      <HomeArticle />
      <HomeSearch />
      <LatestNews />
    </>
  );
};

export default Home;
