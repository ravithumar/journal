import React, { useEffect, useState } from "react";
import PageHeader from "../../components/PageHeader";
import styles from "./author.module.scss";
import { getApi } from "../../utils/ServiceManager";
import useMarkdown from "../../hooks/useMarkdown";
import { Translation } from "react-i18next";
import Loader from "../../components/Loader";

const Author = () => {
  const [data, setData] = useState("");
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    getData();
  }, []);
  const getData = () => {
    setIsLoading(true);
    getApi("for-author", onSuccess, onFailure);
  };
  const onSuccess = (res) => {
    setIsLoading(false);
    if (res?.status) {
      setData(res?.data?.content);
    } else {
      setError(res?.message);
    }
  };
  const onFailure = (err) => {
    setIsLoading(false);
    // setError(JSON.stringify(err?.message));
  };
  const { HTML } = useMarkdown(data);

  return (
    <Translation>
      {(t) => (
        <section className="container mb-5">
          <PageHeader pageTitle={t("Author.header")} />
          <div className="d-flex flex-column my-4 py-3 py-lg-4 px-3 rounded-3 bg-opacity-10 bg-white">
            {isLoading ? (
              <Loader />
            ) : (
              data !== "" && (
                <div
                  className={styles.authorHtml}
                  dangerouslySetInnerHTML={{
                    __html: HTML,
                  }}
                ></div>
              )
            )}
            {/* {error && <h2 className="text-danger text-center">{error}</h2>} */}
          </div>
        </section>
      )}
    </Translation>
  );
};

export default Author;
