import React, { useEffect, useState } from "react";
import styles from "./newsDetails.module.scss";
import separator from "../../assets/images/separator.png";
import { Link, useLocation } from "react-router-dom";
import arrowForward from "../../assets/images/arrowGreen.png";
import Slider from "react-slick";
import nextArrow from "../../assets/images/nextArrow.png";
import previousArrow from "../../assets/images/previousArrow.png";
import { Card } from "react-bootstrap";
import { getApi } from "../../utils/ServiceManager";
import i18next from "i18next";
import Loader from "../../components/Loader";

const NewsDetails = () => {
  let location = useLocation();
  const { pathname } = useLocation();
  const { id } = location?.state;
  const [data, setData] = useState({});
  const [news, setNews] = useState([]);
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    getData();
    getNews();
  }, [pathname]);
  const getData = () => {
    setIsLoading(true);
    getApi(`news/article?id=${id}`, onSuccess, onFailure);
  };
  const onSuccess = (res) => {
    setIsLoading(false);
    if (res?.status) {
      setData(res?.data?.article);
    } else {
      setError(res?.message);
    }
  };
  const onFailure = (err) => {
    setIsLoading(false);
    // setError(JSON.stringify(err?.message));
  };
  const getNews = () => {
    setIsLoading(true);
    getApi("news/home?limit=15", onSuccessNews, onFailureNews);
  };
  const onSuccessNews = (res) => {
    setIsLoading(false);
    if (res?.status) {
      setNews(res?.data?.articles);
    } else {
      setError(res?.message);
    }
  };
  const onFailureNews = (err) => {
    setIsLoading(false);
    // setError(JSON.stringify(err?.message));
  };

  const NextArrow = (props) => {
    return (
      <div
        onClick={props.onClick}
        className={`justify-content-center align-items-center position-relative ${styles.nextIcon}`}
      >
        <img className="d-block w-50" src={nextArrow} alt="First slide" />
      </div>
    );
  };
  const PrevArrow = (props) => {
    return (
      <div
        onClick={props.onClick}
        className={`justify-content-center align-items-center position-relative ${styles.prevIcon}`}
      >
        <img className="d-block w-50" src={previousArrow} alt="First slide" />
      </div>
    );
  };
  const settings = {
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    initialSlide: 0,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <>
          <section className="container my-3 my-md-5">
            <div className={`d-flex flex-column flex-md-row gap-4 gap-lg-5`}>
              <div className={styles.imgDiv}>
                <img
                  className="d-block"
                  src={data?.img_url}
                  alt="First slide"
                />
              </div>
              <div className="col-md-6 d-flex flex-column justify-content-center">
                <h1 className={`${styles.carouselTitle}`}>{data?.title}</h1>
                <p className={`${styles.carouselPara}`}>{data?.description}</p>
                <p className={`${styles.carouselPara}`}>
                  {i18next.language === "ar" ? "كتب بواسطة" : "Written By"}
                </p>
                <p className={`fs-4 opacity-75 ${styles.carouselPara}`}>
                  {data?.author}
                </p>
                <p className={`fs-5 opacity-75 ${styles.carouselPara}`}>
                  {data?.created_at}
                </p>
              </div>
            </div>
          </section>
          <img
            className="d-block w-100 container"
            style={{ height: "1rem" }}
            src={separator}
            alt="Separator"
          />
          <section className="container my-5 px-4">
            <div className="d-flex flex-wrap justify-content-between my-5">
              <div>
                <p className={styles.darkGreen}>{data?.content}</p>
              </div>
            </div>
          </section>
          <section className="bg-white py-5">
            <div className="container">
              <h5 className={`text-center fw-bold ${styles.more} py-lg-4`}>
                {i18next.language === "ar" ? "المزيد من الأخبار" : "More News"}
              </h5>
              <Link
                to={"/news-&-articles"}
                className={` d-flex align-items-center gap-2 mb-2 justify-content-end`}
              >
                <p className={`${styles.link} mb-0 fw-bold`}>
                  {i18next.language === "ar" ? "مشاهدة الكل" : "View All"}
                </p>
                <div className="d-flex">
                  <div className="w-auto mb-1">
                    <img className="d-block w-100" src={arrowForward} alt="" />
                  </div>
                  <div className="w-auto">
                    <img className="d-block w-100" src={arrowForward} alt="" />
                  </div>
                </div>
              </Link>
              {news?.length !== 0 && (
                <div className={`py-5 px-2 ${styles.contactBox}`}>
                  <Slider
                    className={`container d-flex gap-md-2 gap-lg-4 align-items-end`}
                    {...settings}
                  >
                    {news?.map((x) => {
                      return (
                        <Link
                          key={x?.id}
                          to={`/news-&-articles/${x?.id}`}
                          state={{ id: x?.id }}
                          className="bg-transparent border-0 px-2"
                        >
                          <Card.Img
                            className={styles.imgCard}
                            variant="top"
                            src={x?.img_url}
                          />
                          <Card.Body className="p-0 pt-4">
                            <h4 className={`h5 text-white ${styles.titleCard}`}>
                              {x?.title}
                            </h4>
                          </Card.Body>
                        </Link>
                      );
                    })}
                  </Slider>
                </div>
              )}
            </div>
          </section>
        </>
      )}
    </>
  );
};

export default NewsDetails;
