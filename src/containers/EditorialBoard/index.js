import React, { useEffect, useState } from "react";
import styles from "./editorialBoard.module.scss";
import email from "../../assets/images/email.png";
import PageHeader from "../../components/PageHeader";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import i18next from "i18next";
import { getApi } from "../../utils/ServiceManager";
import { Translation } from "react-i18next";
import Loader from "../../components/Loader";

const EditorialBoard = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);
  // const [type, setType] = useState("editorial board");
  useEffect(() => {
    getData("editor-in-chief");
  }, []);
  const getData = (type) => {
    setIsLoading(true);
    getApi(`board-members?type=${type}`, onSuccess, onFailure);
  };
  const onSuccess = (res) => {
    setIsLoading(false);
    if (res?.status) {
      setData(res?.data?.members);
    } else {
      setError(res?.message);
    }
  };
  const onFailure = (err) => {
    setIsLoading(false);
    // setError(JSON.stringify(err?.message));
  };

  return (
    <Translation>
      {(t) => (
        <section className="container">
          <PageHeader pageTitle={t("Header.editorialBoard")} />
          <Tabs direction={i18next.language === "ar" ? "rtl" : "ltr"}>
            <TabList className="d-flex justify-content-center px-0 mt-3 flex-wrap">
              <Tab
                className={`badge rounded-pill fs-6 d-flex align-items-center justify-content-center fw-normal ${styles.pill}`}
                onClick={() => getData("editor-in-chief")}
              >
                {t("EditorialBoard.EditorInChief")}
              </Tab>
              {/* <Tab
                className={`badge rounded-pill fs-6 d-flex align-items-center justify-content-center fw-normal ${styles.pill}`}
                onClick={() => getData("managing editor")}
              >
                {t("EditorialBoard.ManagingEditor")}
              </Tab> */}
              <Tab
                className={`badge rounded-pill fs-6 d-flex align-items-center justify-content-center fw-normal ${styles.pill}`}
                onClick={() => getData("editorial team")}
              >
                {t("EditorialBoard.EditorialTeam")}
              </Tab>
              <Tab
                className={`badge rounded-pill fs-6 d-flex align-items-center justify-content-center fw-normal ${styles.pill}`}
                onClick={() => getData("editorial board")}
              >
                {t("EditorialBoard.EditorialBoard")}
              </Tab>
            </TabList>
            <div className="d-flex flex-column gap-2 mt-2 mb-4">
              {isLoading ? (
                <>
                  <Loader />
                </>
              ) : (
                <>
                  <TabPanel>
                    {data !== [] &&
                      data?.map((x, index) => {
                        return (
                          <div
                            key={index}
                            className={`d-flex flex-column align-items-center my-4 py-5 px-3 rounded-3 ${styles.cardBox}`}
                          >
                            <h3 className="h4 fw-bold text-center mb-3 mb-md-4">
                              {x?.name}
                            </h3>
                            <h4 className="h5 fw-bold mb-3 mb-md-4 text-center">
                              {x?.info}
                            </h4>
                            <a
                              href={`mailto:${x?.email}`}
                              className={`d-flex gap-2 align-items-center text-center ${styles.link}`}
                            >
                              <div style={{ width: "fit-content" }}>
                                <img
                                  className={`d-block ${styles.email}`}
                                  src={email}
                                  alt="EMAIL"
                                />
                              </div>
                              {x?.email}
                            </a>
                          </div>
                        );
                      })}
                    {/* {error && (
                      <h2 className="text-danger text-center">{error}</h2>
                    )} */}
                  </TabPanel>
                  {/* <TabPanel>
                    {data !== [] &&
                      data?.map((x, index) => {
                        return (
                          <div
                            key={index}
                            className={`d-flex flex-column align-items-center my-4 py-5 px-3 rounded-3 ${styles.cardBox}`}
                          >
                            <h3 className="h4 fw-bold text-center mb-3 mb-md-4">
                              {x?.name}
                            </h3>
                            <h4 className="h5 fw-bold mb-3 mb-md-4 text-center">
                              {x?.info}
                            </h4>
                            <a
                              href={`mailto:${x?.email}`}
                              className={`d-flex gap-2 align-items-center text-center ${styles.link}`}
                            >
                              <div style={{ width: "fit-content" }}>
                                <img
                                  className={`d-block ${styles.email}`}
                                  src={email}
                                  alt="EMAIL"
                                />
                              </div>
                              {x?.email}
                            </a>
                          </div>
                        );
                      })}
                    {/* {error && (
                      <h2 className="text-danger text-center">{error}</h2>
                    )} */}
                  {/* </TabPanel> */}
                  <TabPanel>
                    {data !== [] &&
                      data?.map((x, index) => {
                        return (
                          <div
                            key={index}
                            className={`d-flex flex-column align-items-center my-4 py-5 px-3 rounded-3 ${styles.cardBox}`}
                          >
                            <h3 className="h4 fw-bold text-center mb-3 mb-md-4">
                              {x?.name}
                            </h3>
                            <h4 className="h5 fw-bold mb-3 mb-md-4 text-center">
                              {x?.info}
                            </h4>
                            <a
                              href={`mailto:${x?.email}`}
                              className={`d-flex gap-2 align-items-center text-center ${styles.link}`}
                            >
                              <div style={{ width: "fit-content" }}>
                                <img
                                  className={`d-block ${styles.email}`}
                                  src={email}
                                  alt="EMAIL"
                                />
                              </div>
                              {x?.email}
                            </a>
                          </div>
                        );
                      })}
                    {/* {error && (
                      <h2 className="text-danger text-center">{error}</h2>
                    )} */}
                  </TabPanel>
                  <TabPanel>
                    {data !== [] &&
                      data?.map((x, index) => {
                        return (
                          <div
                            key={index}
                            className={`d-flex flex-column align-items-center my-4 py-5 px-3 rounded-3 ${styles.cardBox}`}
                          >
                            <h3 className="h4 fw-bold text-center mb-3 mb-md-4">
                              {x?.name}
                            </h3>
                            <h4 className="h5 fw-bold mb-3 mb-md-4 text-center">
                              {x?.info}
                            </h4>
                            <a
                              href={`mailto:${x?.email}`}
                              className={`d-flex gap-2 align-items-center text-center ${styles.link}`}
                            >
                              <div style={{ width: "fit-content" }}>
                                <img
                                  className={`d-block ${styles.email}`}
                                  src={email}
                                  alt="EMAIL"
                                />
                              </div>
                              {x?.email}
                            </a>
                          </div>
                        );
                      })}
                    {/* {error && (
                      <h2 className="text-danger text-center">{error}</h2>
                    )} */}
                  </TabPanel>
                </>
              )}
            </div>
          </Tabs>
        </section>
      )}
    </Translation>
  );
};

export default EditorialBoard;
