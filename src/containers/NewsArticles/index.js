import React, { useEffect, useState } from "react";
import { Card, Carousel } from "react-bootstrap";
import PageHeader from "../../components/PageHeader";
import styles from "./newsArticles.module.scss";
import nextArrow from "../../assets/images/nextArrow.png";
import previousArrow from "../../assets/images/previousArrow.png";
import { Link } from "react-router-dom";
import { getApi } from "../../utils/ServiceManager";
import axios from "axios";
import i18next from "i18next";
import Loader from "../../components/Loader";
import starBg from "../../assets/images/darkStarBg.png";

const NewsArticles = () => {
  const [category, setCategory] = useState([]);
  const [data, setData] = useState([]);
  const [error, setError] = useState();
  const [isCategoryLoading, setisCategoryLoading] = useState(false);
  const instance = axios.create({
    baseURL: `${process.env.REACT_APP_PROD_API_URL}/${
      i18next.language === "ar" ? "ar" : "en"
    }`,
  });
  instance.defaults.headers.common["Authorization"] = "Bearer password";

  useEffect(() => {
    getCategory();
  }, []);
  const getCategory = () => {
    setisCategoryLoading(true);
    getApi(`news/categories`, onSuccessCategory, onFailureCategory);
  };

  const onSuccessCategory = (res) => {
    setisCategoryLoading(false);
    const getArticle = res?.data?.categories?.map((x, index) => {
      instance
        .get(`news/articles?id=${x?.id}`)
        .then((response) => {
          x["news"] = response?.data?.data?.articles;
          setData((oldData) => [...oldData, response?.data?.data?.articles]);
        })
        .catch((error) => {
          setError(JSON.stringify(error?.message));
        });
      return x;
    });
    setCategory(getArticle);
  };

  const onFailureCategory = (err) => {
    setisCategoryLoading(false);
    // setError(JSON.stringify(err?.message));
  };

  const divideArray = (originalArray, size) => {
    const dividedArray = [];
    let index = 0;
    while (index < originalArray?.length) {
      dividedArray.push(originalArray?.slice(index, index + size));
      index += size; // alternatively you can say index +=size
    }
    return dividedArray;
  };

  return (
    <>
      {category?.length !== 0 && (
        <img
          className={`d-block w-50 position-absolute ${
            i18next.language === "ar" ? styles.imgAr : styles.img
          }`}
          src={starBg}
          alt="First slide"
        />
      )}
      <section className="container position-relative">
        <PageHeader pageTitle={i18next.t("Header.News")} />
        <div className="d-flex flex-column">
          {isCategoryLoading ? (
            <Loader />
          ) : (
            <>
              {category?.length !== 0 &&
                category?.map((x, index) => {
                  if (x?.news?.length === 0) {
                    return <></>;
                  }
                  return (
                    <div key={x?.id} className="my-5">
                      <h2
                        className={`${styles.category} text-center text-md-start fw-bold h1`}
                      >
                        {x?.name}
                      </h2>
                      <Carousel
                        interval={null}
                        indicators={false}
                        nextIcon={
                          <div
                            className={`justify-content-center align-items-center position-absolute ${styles.nextIcon}`}
                          >
                            <img
                              className="d-block w-50"
                              src={nextArrow}
                              alt="First slide"
                            />
                          </div>
                        }
                        prevIcon={
                          <div
                            className={`justify-content-center align-items-center position-absolute ${styles.prevIcon}`}
                          >
                            <img
                              className="d-block w-50"
                              src={previousArrow}
                              alt="First slide"
                            />
                          </div>
                        }
                      >
                        {data?.length === category?.length ? (
                          divideArray(
                            data?.length === category?.length && data[index],
                            3
                          )?.map((y, indexY) => {
                            return (
                              <Carousel.Item key={indexY}>
                                <div
                                  className={`d-flex flex-column flex-lg-row gap-3`}
                                >
                                  {y[0] && (
                                    <div className={styles.left}>
                                      <Card className="p-2 bg-white bg-opacity-10 border-rounded border-0">
                                        <div>
                                          <Card.Img
                                            variant="top"
                                            src={y[0]?.img_url}
                                          />
                                        </div>
                                        <Card.Body style={{ height: "14rem" }}>
                                          <h3
                                            className={`h4 fw-bold ${styles.cGreen} ${styles.articleTitle}`}
                                          >
                                            {y[0]?.title}
                                          </h3>
                                          <Card.Text
                                            className={`opacity-75 ${styles.cGreen} ${styles.articleDesc}`}
                                          >
                                            {y[0]?.description}
                                          </Card.Text>
                                          <div
                                            className={`d-flex justify-content-between opacity-50 ${styles.cDarkGreen}`}
                                          >
                                            <p>
                                              {
                                                divideArray(
                                                  data?.length ===
                                                    category?.length &&
                                                    data[index],
                                                  3
                                                )[0][0]?.created_at
                                              }
                                            </p>
                                            <p>{`By : ${y[0]?.author}`}</p>
                                          </div>
                                          <Link
                                            className="d-flex justify-content-end"
                                            to={`/news-&-articles/${y[0]?.id}`}
                                            state={{
                                              id: y[0]?.id,
                                            }}
                                          >
                                            <p
                                              className={`${styles.cGreen} ${styles.link}`}
                                            >
                                              Read More
                                            </p>
                                          </Link>
                                        </Card.Body>
                                      </Card>
                                    </div>
                                  )}
                                  <div
                                    className={`d-flex gap-4 flex-column ${styles.right}`}
                                  >
                                    {y[1] && (
                                      <Card
                                        className={`p-2 bg-white bg-opacity-10 border-rounded border-0 flex-column flex-md-row w-100 ${styles.rightCard}`}
                                      >
                                        <Card.Img
                                          src={
                                            divideArray(
                                              data?.length ===
                                                category?.length && data[index],
                                              3
                                            )[0][1]?.img_url
                                          }
                                        />
                                        <Card.Body>
                                          <h3
                                            className={`h4 fw-bold ${styles.cGreen} ${styles.articleTitle}`}
                                          >
                                            {y[1]?.title}
                                          </h3>
                                          <Card.Text
                                            className={`opacity-75 ${styles.cGreen} ${styles.articleDescRight}`}
                                          >
                                            {y[1]?.description}
                                          </Card.Text>
                                          <div
                                            className={`d-flex flex-column flex-xxl-row justify-content-between opacity-50 ${styles.cDarkGreen}`}
                                          >
                                            <p>
                                              {
                                                divideArray(
                                                  data?.length ===
                                                    category?.length &&
                                                    data[index],
                                                  3
                                                )[0][1]?.created_at
                                              }
                                            </p>
                                            <p>{`By : ${y[1]?.author}`}</p>
                                          </div>
                                          <Link
                                            to={`/news-&-articles/${y[1]?.id}`}
                                            state={{
                                              id: y[1]?.id,
                                            }}
                                          >
                                            <p
                                              className={`${styles.cGreen} ${styles.link} m-0 pt-md-4 pt-lg-0 pt-xxl-4`}
                                            >
                                              Read More
                                            </p>
                                          </Link>
                                        </Card.Body>
                                      </Card>
                                    )}
                                    {y[2] && (
                                      <Card
                                        className={`p-2 bg-white bg-opacity-10 border-rounded border-0 flex-column flex-md-row w-100 ${styles.rightCard}`}
                                      >
                                        <Card.Img
                                          src={
                                            divideArray(
                                              data?.length ===
                                                category?.length && data[index],
                                              3
                                            )[0][2]?.img_url
                                          }
                                        />
                                        <Card.Body>
                                          <h3
                                            className={`h4 fw-bold ${styles.cGreen} ${styles.articleTitle}`}
                                          >
                                            {y[2]?.title}
                                          </h3>
                                          <Card.Text
                                            className={`opacity-75 ${styles.cGreen} ${styles.articleDescRight}`}
                                          >
                                            {y[2]?.description}
                                          </Card.Text>
                                          <div
                                            className={`d-flex flex-column flex-xxl-row justify-content-between opacity-50 ${styles.cDarkGreen}`}
                                          >
                                            <p>
                                              {
                                                divideArray(
                                                  data?.length ===
                                                    category?.length &&
                                                    data[index],
                                                  3
                                                )[0][2]?.created_at
                                              }
                                            </p>
                                            <p>{`By : ${y[2]?.author}`}</p>
                                          </div>
                                          <Link
                                            to={`/news-&-articles/${y[2]?.id}`}
                                            state={{
                                              id: y[2]?.id,
                                            }}
                                          >
                                            <p
                                              className={`${styles.cGreen} ${styles.link} m-0 pt-md-4 pt-lg-0 pt-xxl-4`}
                                            >
                                              Read More
                                            </p>
                                          </Link>
                                        </Card.Body>
                                      </Card>
                                    )}
                                  </div>
                                </div>
                              </Carousel.Item>
                            );
                          })
                        ) : (
                          <Loader />
                        )}
                      </Carousel>
                    </div>
                  );
                })}
            </>
          )}
        </div>
        {/* {error && <h2 className="text-danger text-center">{error}</h2>} */}
      </section>
    </>
  );
};

export default NewsArticles;
