import { useFormik } from "formik";
import i18next from "i18next";
import React from "react";
import { Translation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import Button from "../../components/Button";
import styles from "./homeSearch.module.scss";

const validate = (values) => {
  const errors = {};
  if (!values.query) {
    errors.query =
      i18next.language === "ar"
        ? "مطلوب اللقب أو الاسم"
        : "Title or Name Required";
  }
  return errors;
};
const HomeSearch = () => {
  const navigate = useNavigate();
  const formik = useFormik({
    initialValues: {
      query: "",
    },
    validate,
    onSubmit: (values) => {
      navigate("/search", {
        state: {
          query: values.query,
        },
      });
    },
  });

  return (
    <Translation>
      {(t) => (
        <>
        {/* <section className="container py-5">
          <div className="d-flex flex-column justify-content-center align-items-center bg-opacity-10 bg-white py-4 rounded-3">
            <h4 className="h4 text-white">{t("Home.SearchTitle")}</h4>
            <p className={`fs-6 ${styles.desc}`}>{t("Home.SearchDesc")}</p>
            <input
              id="query"
              name="query"
              value={formik.values.query}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              type="text"
              className={`form-control bg-transparent px-4 text-white ${styles.searchTextInput}`}
              placeholder={t("Home.SearchPlaceholder")}
            />
            {formik.errors.query && (
              <p className="text-danger mt-2">{formik.errors.query}</p>
            )}
            <Button
              onClick={formik.handleSubmit}
              type="submit"
              text={t("Search")}
              className={`mt-4 ${styles.button}`}
            />
          </div>
        </section> */}
        </>
      )}
    </Translation>
  );
};

export default HomeSearch;
