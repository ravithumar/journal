import React, { useState } from "react";
import { Form } from "react-bootstrap";
import Button from "../../components/Button";
import PageHeader from "../../components/PageHeader";
import styles from "./contact.module.scss";
import { useFormik } from "formik";
import { postApi } from "../../utils/ServiceManager";
import { Translation } from "react-i18next";
import i18next from "i18next";
import locationIcon from "../../assets/images/locationIcon.png";
import msgIcon from "../../assets/images/msgIcon.png";
import web from "../../assets/images/web.png";

const validate = (values) => {
  const errors = {};
  if (!values.name) {
    errors.name = i18next.language === "ar" ? "الاسم مطلوب" : "Name Required";
  } else if (values.name.length > 15) {
    errors.name =
      i18next.language === "ar"
        ? "يجب أن يكون عدد الأحرف 15 حرفًا أو أقل"
        : "Must be 15 characters or less";
  }
  if (!values.email) {
    errors.email =
      i18next.language === "ar" ? "البريد الإلكتروني (مطلوب" : "Email Required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email =
      i18next.language === "ar"
        ? "عنوان البريد الإلكتروني غير صالح"
        : "Invalid email address";
  }
  if (!values.subject) {
    errors.subject =
      i18next.language === "ar" ? "الموضوع (مطلوب" : "Subject Required";
  }
  if (!values.message) {
    errors.message =
      i18next.language === "ar" ? "الرسالة المطلوبة" : "Message Required";
  }
  return errors;
};
const Contact = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();
  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      subject: "",
      message: "",
    },
    validate,
    onSubmit: (values, { resetForm }) => {
      setIsLoading(true);
      postApi(
        "contact-us",
        values,
        (res) => {
          setIsLoading(false);
          setError(res?.message);
          if (res?.status) {
            resetForm({ values: "" });
          }
        },
        (err) => {
          setIsLoading(false);
          setError(err?.message.toString());
        }
      );
    },
  });

  return (
    <Translation>
      {(t) => (
        <section className="container mb-5">
          <PageHeader pageTitle={t("Header.contactUs")} />
          <div className="my-5 d-flex flex-column flex-lg-row gap-4 justify-content-center">
            <div
              className={`col-12 col-md-5 bg-white bg-opacity-10 p-3 p-md-5 rounded-3 d-flex flex-column ${styles.box}`}
            >
              <iframe
                title="Contact"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d115443.09541919112!2d51.44195721329916!3d25.284147787565892!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e45c534ffdce87f%3A0x44d9319f78cfd4b1!2sDoha%2C%20Qatar!5e0!3m2!1sen!2sin!4v1643287779988!5m2!1sen!2sin"
                // width="600"
                height="500"
                style={{ border: 0 }}
                allowFullScreen=""
                loading="lazy"
                className="embed-responsive-item"
              ></iframe>
            </div>
            <div
              className={`bg-white bg-opacity-10 p-3 p-md-5 rounded-3 d-flex flex-column ${styles.box}`}
            >
              <Form noValidate>
                <Form.Group className="mb-3">
                  <Form.Label className={styles.label}>
                    {t("Form.YourName")}
                  </Form.Label>
                  <Form.Control
                    id="name"
                    name="name"
                    value={formik.values.name}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    className={`rounded-3 bg-transparent ${styles.input}`}
                    placeholder={t("Form.EnterName")}
                  />
                  <Form.Text className="text-danger">
                    {formik.errors.name}
                  </Form.Text>
                </Form.Group>
                <Form.Group className="mb-3">
                  <Form.Label className={styles.label}>
                    {t("Form.ContactEmail")}
                  </Form.Label>
                  <Form.Control
                    id="email"
                    name="email"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    className={`rounded-3 bg-transparent w-50 ${styles.input}`}
                    type="email"
                    placeholder={t("Form.EnterEmail")}
                  />
                  <Form.Text className="text-danger">
                    {formik.errors.email}
                  </Form.Text>
                </Form.Group>
                <Form.Group className="mb-3">
                  <Form.Label className={styles.label}>
                    {t("Form.Subject")}
                  </Form.Label>
                  <Form.Control
                    id="subject"
                    name="subject"
                    value={formik.values.subject}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    className={`rounded-3 bg-transparent ${styles.input}`}
                    placeholder={t("Form.EnterSubject")}
                  />
                  <Form.Text className="text-danger">
                    {formik.errors.subject}
                  </Form.Text>
                </Form.Group>
                <Form.Group className="mb-3">
                  <Form.Label className={styles.label}>
                    {t("Form.YourMessage")}
                  </Form.Label>
                  <Form.Control
                    id="message"
                    name="message"
                    value={formik.values.message}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    className={`rounded-3 bg-transparent ${styles.input}`}
                    as="textarea"
                    style={{ height: "100px" }}
                    placeholder={t("Form.EnterMessage")}
                  />
                  <Form.Text className="text-danger">
                    {formik.errors.message}
                  </Form.Text>
                </Form.Group>
                <Button
                  disabled={isLoading}
                  onClick={formik.handleSubmit}
                  type="submit"
                  className={`text-white w-100 mt-2 mt-md-4 ${styles.btnSubmit}`}
                  text={
                    isLoading ? (
                      <>
                        <span
                          className="spinner-border spinner-border-sm mx-3"
                          role="status"
                          aria-hidden="true"
                        ></span>
                        {i18next.language === "ar"
                          ? "جار التحميل..."
                          : "Loading..."}
                      </>
                    ) : (
                      t("Submit")
                    )
                  }
                />
              </Form>
              {error && (
                <h2 className="text-black text-center mt-3">{error}</h2>
              )}
            </div>
          </div>
          <div className="d-flex flex-column flex-lg-row justify-content-between gap-4">
            <div className="d-flex gap-3 text-white">
              <div className={styles.roundedIcon}>
                <img src={locationIcon} alt="Location" />
              </div>
              <div>
                <p className="opacity-75">Address</p>
                <a
                  target={"_blank"}
                  rel="noopener noreferrer"
                  href="https://www.google.com/maps/place/Doha,+Qatar/@25.2840091,51.5119967,12z/data=!4m2!3m1!1s0x3e45c534ffdce87f:0x44d9319f78cfd4b1"
                  style={{ color: "inherit" }}
                  className="h5"
                >
                  General Directorate of Endowments Ministry
                  <br /> OF Endowment and Islamic Affairs
                  <br /> PO Box xx, xxxxx St, Doha, Qatar
                </a>
              </div>
            </div>
            <div className="d-flex gap-3 text-white">
              <div className={styles.roundedIcon}>
                <img src={msgIcon} alt="Location" />
              </div>
              <div>
                <p className="opacity-75">Email</p>
                <a
                  href="mailto:xxxxxxxxxxxx"
                  className="h5"
                  style={{ color: "inherit" }}
                >
                  xxxxxxxxxx
                </a>
              </div>
            </div>
            <div className="d-flex gap-3 text-white">
              <div className={styles.roundedIcon}>
                <img src={web} alt="Location" />
              </div>
              <div>
                <p className="opacity-75">Website</p>
                <a
                  target={"_blank"}
                  rel="noopener noreferrer"
                  href="https://awqafjournal.com/"
                  style={{ color: "inherit" }}
                  className="h5"
                >
                  awqafjournal.com
                </a>
              </div>
            </div>
          </div>
        </section>
      )}
    </Translation>
  );
};

export default Contact;
