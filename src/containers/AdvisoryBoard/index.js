import React, { useEffect, useState } from "react";
import styles from "./advisoryBoard.module.scss";
import email from "../../assets/images/email.png";
import PageHeader from "../../components/PageHeader";
import { getApi } from "../../utils/ServiceManager";
import { Translation } from "react-i18next";
import Loader from "../../components/Loader";

const AdvisoryBoard = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    getData();
  }, []);
  const getData = () => {
    setIsLoading(true);
    getApi("board-members?type=advisory board", onSuccess, onFailure);
  };
  const onSuccess = (res) => {
    setIsLoading(false);
    if (res?.status) {
      setData(res?.data?.members);
    } else {
      setError(res?.message);
    }
  };
  const onFailure = (err) => {
    setIsLoading(false);
    // setError(JSON.stringify(err?.message));
  };
  return (
    <Translation>
      {(t) => (
        <section className="container">
          <PageHeader pageTitle={t("Header.advisoryBoard")} />
          <div className="d-flex justify-content-between flex-wrap mt-3 mb-4">
            {isLoading ? (
              <Loader />
            ) : (
              <>
                {data !== [] &&
                  data?.map((x, index) => {
                    return (
                      <div
                        key={index}
                        className={`col-12 col-md-5 d-flex flex-column align-items-center my-4 py-3 py-lg-4 px-3 rounded-3 ${styles.cardBox}`}
                      >
                        <h3 className="h4 fw-bold mb-3 text-center">
                          {x?.name}
                        </h3>
                        <h4 className="h6 fw-bold mb-3 mb-xxl-4 text-center">
                          {x?.info}
                        </h4>
                        <a
                          href={`mailto:${x?.email}`}
                          className={`d-flex gap-2 align-items-center my-2 my-lg-3 my-xxl-4 ${styles.link}`}
                        >
                          <div style={{ width: "fit-content" }}>
                            <img
                              className={`d-block ${styles.email}`}
                              src={email}
                              alt="EMAIL"
                            />
                          </div>
                          {x?.email}
                        </a>
                      </div>
                    );
                  })}
              </>
            )}
          </div>
          {/* {error && <h2 className="text-danger text-center">{error}</h2>} */}
        </section>
      )}
    </Translation>
  );
};

export default AdvisoryBoard;
