import React, { useEffect, useState } from "react";
import styles from "./journal.module.scss";
import mosque from "../../assets/images/mosque-malaysia-sunset.png";
import PageHeader from "../../components/PageHeader";
import { getApi } from "../../utils/ServiceManager";
import { Translation } from "react-i18next";
import Loader from "../../components/Loader";
import starBg from "../../assets/images/darkStarBg.png";
import i18next from "i18next";

const Journal = () => {
  const [data, setData] = useState({});
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    getData();
  }, []);
  const getData = () => {
    setIsLoading(true);
    getApi("about", onSuccess, onFailure);
  };
  const onSuccess = (res) => {
    setIsLoading(false);
    if (res?.status) {
      setData(res?.data);
    } else {
      setError(res?.message);
    }
  };
  const onFailure = (err) => {
    setIsLoading(false);
    // setError(JSON.stringify(err?.message));
  };
  return (
    <Translation>
      {(t) => (
        <>
          <img
            className={`d-block position-absolute ${
              i18next.language === "ar" ? styles.imgAr : styles.img
            }`}
            src={starBg}
            alt="First slide"
          />
          <section className="container mb-5">
            <PageHeader pageTitle={t("About.header")} />
            <div className="d-flex flex-column mt-5 gap-4">
              {/* <div className="col-md-6 d-flex flex-column justify-content-center gap-2">
                <h2 className={`h2 ${styles.subTitle}`}>{t("About.Bait")}</h2>
                <div>
                  <p className={styles.details}>{t("About.desc1")}</p>
                  <p className={styles.details}>{t("About.desc2")}</p>
                  <p className={styles.details}>{t("About.desc3")}</p>
                </div>
              </div> */}
              {isLoading ? (
                <Loader />
              ) : (
                data !== {} && (
                  <div className="d-flex flex-column-reverse flex-md-row container p-3 p-md-4 p-xl-5 shadow rounded bg-white">
                    <div>
                      <h3 className={`h3 ${styles.aboutSubTitle}`}>
                        {t("About.About")}
                      </h3>
                      <p className={`p ${styles.aboutdesc}`}>{data?.about}</p>
                      <h3 className={`h3 ${styles.aboutSubTitle}`}>
                        {t("About.Vision")}
                      </h3>
                      <p className={`p ${styles.aboutdesc}`}>{data?.vision}</p>
                      <h3 className={`h3 ${styles.aboutSubTitle}`}>
                        {t("About.Message")}
                      </h3>
                      <p className={`p ${styles.aboutdesc}`}>{data?.message}</p>
                      <h3 className={`h3 ${styles.aboutSubTitle}`}>
                        {t("About.Objectives")}
                      </h3>
                      <p className={`p ${styles.aboutdesc}`}>{data?.goal}</p>
                      <h3 className={`h3 ${styles.aboutSubTitle}`}>
                        {t("About.Values")}
                      </h3>
                      <p className={`p ${styles.aboutdesc}`}>{data?.values}</p>
                    </div>
                    <div className="position-relative d-flex col-12 col-md-5">
                      <div className={styles.imgOverlay}>
                        <img
                          className="d-block"
                          src={mosque}
                          alt="mosque"
                          style={{ zIndex: 1 }}
                        />
                      </div>
                    </div>
                  </div>
                )
              )}
              {/* {error && <h2 className="text-danger text-center">{error}</h2>} */}
            </div>
          </section>
        </>
      )}
    </Translation>
  );
};

export default Journal;
