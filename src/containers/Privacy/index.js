import React, { useEffect, useState } from "react";
import { Translation } from "react-i18next";
import Loader from "../../components/Loader";
import PageHeader from "../../components/PageHeader";
import useMarkdown from "../../hooks/useMarkdown";
import { getApi } from "../../utils/ServiceManager";
import styles from "./privacy.module.scss";

const Privacy = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    getData();
  }, []);
  const getData = () => {
    setIsLoading(true);
    getApi(`privacy-policy`, onSuccess, onFailure);
  };
  const onSuccess = (res) => {
    setIsLoading(false);
    if (res?.status) {
      setData(res?.data?.content);
    } else {
      setError(res?.message);
    }
  };
  const onFailure = (err) => {
    setIsLoading(false);
    // setError(JSON.stringify(err?.message));
  };
  const { HTML } = useMarkdown(data);

  return (
    <Translation>
      {(t) => (
        <section className="container">
          <PageHeader pageTitle={t("Footer.PrivacyPolicy")} />
          <div className="my-4 py-3 py-lg-5 px-3 px-md-4 px-lg-5 rounded-3 bg-opacity-10 bg-white">
            {isLoading ? (
              <Loader />
            ) : (
              data !== "" && (
                <div
                  className={styles.privacyHtml}
                  dangerouslySetInnerHTML={{
                    __html: HTML,
                  }}
                ></div>
              )
            )}
          </div>
          {/* {error && <h2 className="text-danger text-center">{error}</h2>} */}
        </section>
      )}
    </Translation>
  );
};

export default Privacy;
