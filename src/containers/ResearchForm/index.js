import React, { useRef, useState } from "react";
import styles from "./researchForm.module.scss";
import logo from "../../assets/images/logo2.png";
import { Form } from "react-bootstrap";
import separator from "../../assets/images/separator2.png";
import pdfUpload from "../../assets/images/pdfUpload.png";
import { useFormik } from "formik";
import i18next from "i18next";
import { useTranslation } from "react-i18next";
import { postApi } from "../../utils/ServiceManager";
import * as Yup from "yup";
import logo4 from "../../assets/images/logo4.png";

const ResearchForm = () => {
  const { i18n } = useTranslation();
  const hiddenFileInputSearch = useRef(null);
  const hiddenFileInputForm = useRef(null);
  const hiddenFileInputDeclaration = useRef(null);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();
  const handleChangeSearch = (event) => {
    const fileUploaded = event.target.files[0];
    formik.setFieldValue("research_file", fileUploaded);
  };
  const handleChangeForm = (event) => {
    const fileUploaded = event.target.files[0];
    formik.setFieldValue("policy_file", fileUploaded);
  };
  const handleChangeDeclaration = (event) => {
    const fileUploaded = event.target.files[0];
    formik.setFieldValue("info_file", fileUploaded);
  };
  const handleClickSearch = (event) => {
    event.preventDefault();
    hiddenFileInputSearch.current.click();
  };
  const handleClickForm = (event) => {
    event.preventDefault();
    hiddenFileInputForm.current.click();
  };
  const handleClickDeclaration = (event) => {
    event.preventDefault();
    hiddenFileInputDeclaration.current.click();
  };
  const SUPPORTED_FORMATS = [
    "application/pdf",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
  ];
  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      title: "",
      description: "",
      research_file: undefined,
      policy_file: undefined,
      info_file: undefined,
    },
    validationSchema: Yup.object({
      name: Yup.string()
        .trim()
        .max(
          15,
          i18next.language === "ar"
            ? "يجب أن يكون عدد الأحرف 15 حرفًا أو أقل"
            : "Must be 15 characters or less"
        )
        .required(i18next.language === "ar" ? "الاسم مطلوب" : "Name Required"),
      email: Yup.string()
        .matches(
          /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
          i18next.language === "ar"
            ? "عنوان البريد الإلكتروني غير صالح"
            : "Invalid email address"
        )
        .required(
          i18next.language === "ar"
            ? "البريد الإلكتروني (مطلوب"
            : "Email Required"
        ),
      title: Yup.string()
        .trim()
        .required(
          i18next.language === "ar" ? "العنوان مطلوب" : "Title Required"
        ),
      description: Yup.string()
        .trim()
        .required(
          i18next.language === "ar" ? "الوصف مطلوب" : "Description Required"
        ),
      research_file: Yup.mixed()
        .required(
          i18next.language === "ar" ? "مطلوب ملف البحث" : "Search File Required"
        )
        .test(
          "fileFormat",
          i18next.language === "ar" ? "صيغة غير مدعومة" : "Unsupported Format",
          (value) => value && SUPPORTED_FORMATS.includes(value.type)
        ),
      policy_file: Yup.mixed()
        .required(
          i18next.language === "ar" ? "ملف النموذج مطلوب" : "Form File Required"
        )
        .test(
          "fileFormat",
          i18next.language === "ar" ? "صيغة غير مدعومة" : "Unsupported Format",
          (value) => value && SUPPORTED_FORMATS.includes(value.type)
        ),
      info_file: Yup.mixed()
        .required(
          i18next.language === "ar"
            ? "مطلوب ملف إقرار"
            : "Declaration File Required"
        )
        .test(
          "fileFormat",
          i18next.language === "ar" ? "صيغة غير مدعومة" : "Unsupported Format",
          (value) => value && SUPPORTED_FORMATS.includes(value.type)
        ),
    }),
    onSubmit: (values, { resetForm }) => {
      setIsLoading(true);
      const formData = new FormData();
      formData.append("research_file", values.research_file);
      formData.append("policy_file", values.policy_file);
      formData.append("info_file", values.info_file);
      formData.append("name", values.name);
      formData.append("email", values.email);
      formData.append("title", values.title);
      formData.append("description", values.description);
      postApi(
        "issues/add",
        formData,
        (res) => {
          setIsLoading(false);
          setError(res?.message);
          if (res?.status) {
            resetForm({ values: "" });
          }
        },
        (err) => {
          setIsLoading(false);
          setError(err?.message.toString());
        }
      );
    },
  });
  return (
    <section className={`container position-relative ${styles.section}`}>
      <div
        className={`w-100 h-50 p-3 p-md-5 bg-white bg-opacity-10 my-auto d-flex flex-column justify-content-between justify-content-xl-around ${styles.horizontalRectangle}`}
      >
        <div>
          <img src={logo4} className={styles.logo} alt="Logo" />
        </div>
        {/* <h1 className={`h4 opacity-50 ${styles.subTitle}`}>
          {i18n.t("document_title")}
        </h1> */}
        {/* <h2 className={styles.title}>{i18n.t("Research.desc")}</h2> */}
        <h2 className={styles.title}>
          <ul className="list-unstyled d-flex flex-column gap-3">
            <li>
              {i18next.language === "ar"
                ? "تُرسل البحوث إلكترونياً إلى "
                : "Researches shall be sent electronically to the "}
              <strong>
                {i18next.language === "ar" ? "مجلة الوقف" : "AL Waqf Journal"}
              </strong>{" "}
              {i18next.language === "ar" ? "من خلال :" : "by :"}
            </li>
            <li>
              <strong>
                {i18next.language === "ar"
                  ? "الطريقة الأولى :"
                  : "First method :"}
              </strong>
            </li>
            <li>
              {i18next.language === "ar"
                ? " رفع ملف البحث بصيغة (الوورد) بملئ النموذج الظاهر في الشاشة"
                : "Uploading the article in Word format by filling out the form that appears on the screen"}
            </li>
            <li>
              <strong>
                {i18next.language === "ar"
                  ? "الطريقة الثانية :"
                  : "Second method :"}
              </strong>
            </li>
            <li>
              {i18next.language === "ar"
                ? "إرسال نسخة من البحث إلى بريد المجلة. "
                : "Send a copy of the article to the journal by email."}
            </li>
          </ul>
        </h2>
      </div>
      <div
        className={`h-100 bg-white top-0 p-3 px-md-5 pt-md-5 pb-0 d-flex flex-column justify-content-center ${
          i18next.language === "ar"
            ? styles.verticalRectangleAr
            : styles.verticalRectangle
        }`}
      >
        <h3
          className={`fw-bold mb-5 text-decoration-underline ${styles.subTitle}`}
        >
          {i18n.t("SubmitHere")}
        </h3>
        <Form noValidate>
          <Form.Group className="mb-3">
            <Form.Label className={styles.label}>
              {i18n.t("Research.Name")}
            </Form.Label>
            <Form.Control
              id="name"
              name="name"
              value={formik.values.name}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              className={`rounded-3 bg-transparent ${styles.input}`}
              placeholder={i18n.t("Form.EnterName")}
            />
            <Form.Text className="text-danger">{formik.errors.name}</Form.Text>
          </Form.Group>

          <Form.Group className="mb-3">
            <Form.Label className={styles.label}>
              {i18n.t("Research.Email")}
            </Form.Label>
            <Form.Control
              id="email"
              name="email"
              value={formik.values.email}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              className={`rounded-3 bg-transparent ${styles.input}`}
              type="email"
              placeholder={i18n.t("Form.EnterEmail")}
            />
            <Form.Text className="text-danger">{formik.errors.email}</Form.Text>
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label className={styles.label}>
              {i18n.t("Research.RTitle")}
            </Form.Label>
            <Form.Control
              id="title"
              name="title"
              value={formik.values.title}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              className={`rounded-3 bg-transparent ${styles.input}`}
              placeholder={i18n.t("Research.RTitlePlace")}
            />
            <Form.Text className="text-danger">{formik.errors.title}</Form.Text>
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label className={styles.label}>
              {i18n.t("Research.RDesc")}
            </Form.Label>
            <Form.Control
              id="description"
              name="description"
              value={formik.values.description}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              className={`rounded-3 bg-transparent ${styles.input}`}
              as="textarea"
              style={{ height: "200px" }}
              placeholder={i18n.t("Research.RDescPLace")}
            />
            <Form.Text className="text-danger">
              {formik.errors.description}
            </Form.Text>
          </Form.Group>
          <div className="d-flex flex-wrap justify-content-between my-5">
            <Form.Group
              className={`d-flex flex-column align-items-center p-3 my-2 my-md-0 p-lg-2 rounded-3 ${styles.group}`}
            >
              <Form.Label className={styles.label}>
                {i18n.t("Research.ASeach")}
              </Form.Label>
              <img className="d-block w-100" src={separator} alt="Separator" />
              <div className="my-3">
                <img
                  className="d-block w-100"
                  src={pdfUpload}
                  alt="pdfUpload"
                />
              </div>
              <button
                disabled={isLoading}
                className={`btn fs-6 ${styles.regularButton}`}
                onClick={handleClickSearch}
              >
                {i18n.t("Research.ChooseFile")}
              </button>
              <input
                accept="application/pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                id="research_file"
                name="research_file"
                type="file"
                required={true}
                style={{ display: "none" }}
                ref={hiddenFileInputSearch}
                onChange={handleChangeSearch}
              />
              <p className={`mb-0 mt-3 ${styles.footText}`}>
                {i18n.t("Research.Format")}
              </p>
              {formik?.values?.research_file && (
                <Form.Text className="text-muted">
                  {formik?.values?.research_file?.name}
                </Form.Text>
              )}
              {formik.errors.research_file && (
                <Form.Text className="text-danger">
                  {formik.errors.research_file}
                </Form.Text>
              )}
            </Form.Group>
            <Form.Group
              className={`d-flex flex-column align-items-center p-3 my-2 my-md-0 p-lg-2 rounded-3 ${styles.group}`}
            >
              <Form.Label className={styles.label}>
                {i18n.t("Research.AForm")}
              </Form.Label>
              <img className="d-block w-100" src={separator} alt="Separator" />
              <div className="my-3">
                <img
                  className="d-block w-100"
                  src={pdfUpload}
                  alt="pdfUpload"
                />
              </div>
              <button
                disabled={isLoading}
                className={`btn fs-6 ${styles.regularButton}`}
                onClick={handleClickForm}
              >
                {i18n.t("Research.ChooseFile")}
              </button>
              <input
                accept="application/pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                id="policy_file"
                name="policy_file"
                type="file"
                required
                style={{ display: "none" }}
                ref={hiddenFileInputForm}
                onChange={handleChangeForm}
              />
              <p className={`mb-0 mt-3 ${styles.footText}`}>
                {i18n.t("Research.Format")}
              </p>
              {formik?.values?.policy_file && (
                <Form.Text className="text-muted">
                  {formik?.values?.policy_file?.name}
                </Form.Text>
              )}
              {formik.errors.policy_file && (
                <Form.Text className="text-danger">
                  {formik.errors.policy_file}
                </Form.Text>
              )}
            </Form.Group>
            <Form.Group
              className={`d-flex flex-column align-items-center p-3 my-2 my-md-0 p-lg-2 rounded-3 ${styles.group}`}
            >
              <Form.Label className={styles.label}>
                {i18n.t("Research.ADecl")}
              </Form.Label>
              <img className="d-block w-100" src={separator} alt="Separator" />
              <div className="my-3">
                <img
                  className="d-block w-100"
                  src={pdfUpload}
                  alt="pdfUpload"
                />
              </div>
              <button
                disabled={isLoading}
                className={`btn fs-6 ${styles.regularButton}`}
                onClick={handleClickDeclaration}
              >
                {i18n.t("Research.ChooseFile")}
              </button>
              <input
                accept="application/pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                id="info_file"
                name="info_file"
                type="file"
                required
                style={{ display: "none" }}
                ref={hiddenFileInputDeclaration}
                onChange={handleChangeDeclaration}
              />
              <p className={`mb-0 mt-3 ${styles.footText}`}>
                {i18n.t("Research.Format")}
              </p>
              {formik?.values?.info_file && (
                <Form.Text className="text-muted">
                  {formik?.values?.info_file?.name}
                </Form.Text>
              )}
              {formik.errors.info_file && (
                <Form.Text className="text-danger">
                  {formik.errors.info_file}
                </Form.Text>
              )}
            </Form.Group>
          </div>
          <button
            disabled={isLoading}
            onClick={formik.handleSubmit}
            type="submit"
            className={`mt-2 mt-md-4 w-100 fs-4 border-transparent btn ${styles.btnSubmit}`}
          >
            {isLoading ? (
              <>
                <span
                  className="spinner-border spinner-border-sm mx-3"
                  role="status"
                  aria-hidden="true"
                ></span>
                {i18next.language === "ar" ? "جار التحميل..." : "Loading..."}
              </>
            ) : (
              i18n.t("Submit")
            )}
          </button>
        </Form>
        {error && <h2 className="text-black text-center mt-3">{error}</h2>}
        <div className="d-flex justify-content-center mt-4">
          <p className={`fs-5 fw-bold m-0 ${styles.subTitle}`}>
            {i18n.t("Research.Or")}
          </p>
        </div>
        <div
          className={`d-flex justify-content-center align-items-center ${styles.mailBox}`}
        >
          <a href="mailto:info@mashurajournal.com">
            <p className="fs-4 m-0 text-white text-center">
              {i18n.t("Research.Footer")}
            </p>
          </a>
        </div>
      </div>
      <div
        className={`position-absolute w-50 h-100 bg-white bg-opacity-10 ${
          i18next.language === "ar"
            ? styles.verticalRectangleCloneAr
            : styles.verticalRectangleClone
        }`}
      ></div>
    </section>
  );
};

export default ResearchForm;
