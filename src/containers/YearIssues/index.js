import React, { useEffect, useState } from "react";
import PageHeader from "../../components/PageHeader";
import styles from "./yearIssues.module.scss";
import user from "../../assets/images/user.png";
import calendar from "../../assets/images/calendar.png";
import { Link, useLocation, useParams } from "react-router-dom";
import { getApi } from "../../utils/ServiceManager";
import i18next from "i18next";
import Loader from "../../components/Loader";
import fileDownload from "js-file-download";
import axios from "axios";

const YearIssues = () => {
  const { name } = useParams();
  let location = useLocation();
  const { id } = location?.state;
  const [data, setData] = useState([]);
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    getData();
  }, []);
  const getData = () => {
    setIsLoading(true);
    getApi(`issues/articles?id=${id}`, onSuccess, onFailure);
  };
  const onSuccess = (res) => {
    setIsLoading(false);
    if (res?.status) {
      if (res?.data?.articles?.length === 0) {
        setData([]);
        setError(i18next.language === "ar" ? "لاتوجد بيانات" : "No data found");
      } else {
        setData(res?.data?.articles);
        setError();
      }
    } else {
      setError(res?.message);
    }
  };
  const onFailure = (err) => {
    setIsLoading(false);
    // setError(JSON.stringify(err?.message));
  };
  const download = (link, title) => {
    const instance = axios.create({
      baseURL: `${process.env.REACT_APP_PROD_API_URL}/${
        i18next.language === "ar" ? "ar" : "en"
      }`,
    });
    instance.defaults.headers.common["Authorization"] = "Bearer password";
    instance
      .get(link, { responseType: "blob" })
      .then((response) => {
        fileDownload(response?.data, `${title}.pdf`);
      })
      .catch((error) => {
        onFailure(error);
      });
  };
  return (
    <section className="container mb-4">
      <PageHeader pageTitle={name} />
      <div className="d-flex flex-wrap justify-content-between">
        {isLoading ? (
          <Loader />
        ) : (
          data.length !== 0 &&
          data?.map((x) => {
            return (
              <div
                key={x?.id}
                className={`d-flex flex-column justify-content-center align-items-center my-4 p-3 p-md-4 rounded-3 ${styles.box}`}
              >
                <Link
                  to={`/archive/${name}/${x?.id}/${x?.title
                    ?.toLowerCase()
                    .replace(/ /g, "-")}`}
                >
                  <h2 className={`h5 fw-bold ${styles.title}`}>{x?.title}</h2>
                </Link>
                <div className={`w-100 py-3 ${styles.boxDesc}`}>
                  <div
                    className={`d-flex flex-column flex-md-row gap-3 gap-md-5 mb-3`}
                  >
                    <div className="d-flex align-items-center gap-2">
                      <div>
                        <img className="d-block w-100" src={user} alt="user" />
                      </div>
                      <p className={`m-0 text-black ${styles.author}`}>
                        {x?.author}
                      </p>
                    </div>
                    <div className="d-flex align-items-center gap-2">
                      <div>
                        <img
                          className="d-block w-100"
                          src={calendar}
                          alt="calendar"
                        />
                      </div>
                      <p className={`m-0 text-black`}>{x?.created_at}</p>
                    </div>
                  </div>
                  <p className={`text-black mb-3 ${styles.pLink}`}>
                    <a href={x?.doi_link}>{x?.doi_link}</a> -{" "}
                    {i18next.language === "ar"
                      ? "مجلة الوقف"
                      : "Al Wagt Journal"}{" "}
                    - {x?.page_no}
                  </p>
                  <p className={`text-black mb-0 ${styles.pLink}`}>
                    This work and the related PDF file are licensed under a
                    <a href="https://creativecommons.org/licenses/by/4.0/">
                      {" "}
                      Creative Commons Attribution 4.0 International
                    </a>
                  </p>
                </div>
                <div className={`d-flex w-100 ${styles.boxDesc}`}>
                  <div className="d-flex flex-column flex-md-row w-100 mt-3 justify-content-between gap-2 gap-md-0">
                    <div className="d-flex gap-3 gap-lg-1 gap-xxl-3 align-items-center">
                      <p
                        className={`fw-bold text-black m-0 ${styles.footText}`}
                      >
                        VIEWS :{" "}
                        <span className={styles.gold}>{x?.read_count}</span>
                      </p>
                      <p
                        className={`fw-bold text-black m-0 ${styles.footText}`}
                      >
                        Pdf-Downloads :{" "}
                        <span className={styles.gold}>{x?.download_count}</span>
                      </p>
                    </div>
                    <div className="d-flex gap-3">
                      <a>
                        <span
                          onClick={() => download(x?.download_link, x?.title)}
                          className={`badge rounded-pill px-4 px-lg-2 px-xxl-4 fs-6 d-flex align-items-center justify-content-center ${styles.pill}`}
                        >
                          Full Text.Pdf
                        </span>
                      </a>
                      <Link
                        to={`/archive/${name}/${x?.id}/${x?.title
                          ?.toLowerCase()
                          .replace(/ /g, "-")}`}
                      >
                        <span
                          className={`badge rounded-pill px-4 px-lg-2 px-xxl-4 fs-6 d-flex align-items-center justify-content-center ${styles.pill}`}
                        >
                          Html
                        </span>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            );
          })
        )}
      </div>
      {/* {error && <h2 className="text-danger text-center my-4">{error}</h2>} */}
    </section>
  );
};

export default YearIssues;
