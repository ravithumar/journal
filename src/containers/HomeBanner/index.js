import React, { useEffect, useState } from "react";
import { Carousel } from "react-bootstrap";
import { Translation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import nextArrow from "../../assets/images/nextArrow.png";
import previousArrow from "../../assets/images/previousArrow.png";
import Button from "../../components/Button";
import styles from "./homeBanner.module.scss";
import i18next from "i18next";
import { getApi } from "../../utils/ServiceManager";
import Loader from "../../components/Loader";

const HomeBanner = () => {
  const navigate = useNavigate();
  const [data, setData] = useState([]);
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    getData();
  }, []);
  const getData = () => {
    setIsLoading(true);
    getApi("news/home?limit=5", onSuccess, onFailure);
  };
  const onSuccess = (res) => {
    setIsLoading(false);
    if (res?.status) {
      setData(res?.data?.articles);
    } else {
      setError(res?.message);
    }
  };
  const onFailure = (err) => {
    setIsLoading(false);
    // setError(JSON.stringify(err?.message));
  };
  return (
    <>
    {/* <Translation>
      {(t) => (
        <section
          className="container"
          style={{ marginTop: "4rem", marginBottom: "5rem" }}
        >
          {isLoading ? (
            <Loader />
          ) : (
            <>
              {data?.length !== 0 && (
                <Carousel
                  interval={null}
                  indicators={false}
                  nextIcon={
                    <div
                      className={`justify-content-center align-items-center position-absolute ${
                        i18next.language === "ar"
                          ? styles.nextIconAr
                          : styles.nextIcon
                      }`}
                    >
                      <img
                        className="d-block w-50"
                        src={nextArrow}
                        alt="First slide"
                      />
                    </div>
                  }
                  prevIcon={
                    <div
                      className={`justify-content-center align-items-center position-absolute ${
                        i18next.language === "ar"
                          ? styles.prevIconAr
                          : styles.prevIcon
                      }`}
                    >
                      <img
                        className="d-block w-50"
                        src={previousArrow}
                        alt="First slide"
                      />
                    </div>
                  }
                >
                  {data?.map((x) => {
                    return (
                      <Carousel.Item key={x?.id}>
                        <div
                          className={`d-flex flex-column flex-md-row gap-4 gap-lg-5 align-items-md-center ${styles.gapCarousel}`}
                        >
                          <div className={styles.imgDiv}>
                            <img
                              className="d-block"
                              src={x?.img_url}
                              alt="First slide"
                            />
                          </div>
                          <div className="col-md-6 d-flex flex-column justify-content-center">
                            <h2 className={`h2 ${styles.carouselTitle}`}>
                              {x?.title}
                            </h2>
                            <Button
                              text={t("SeeMore")}
                              onClick={() =>
                                navigate(`/news-&-articles/${x?.id}`, {
                                  state: {
                                    id: x?.id,
                                  },
                                })
                              }
                              className={`mt-4 ${styles.button}`}
                            />
                          </div>
                        </div>
                      </Carousel.Item>
                    );
                  })}
                </Carousel>
              )}
            </>
          )}
        </section>
      )}
    </Translation> */}
    </>
  );
};

export default HomeBanner;
