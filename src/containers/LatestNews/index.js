import React, { useEffect, useState } from "react";
import styles from "./latestNews.module.scss";
import nextArrow from "../../assets/images/arrow-right.svg";
import previousArrow from "../../assets/images/arrow-left.svg";
import { Card } from "react-bootstrap";
import arrowForward from "../../assets/images/arrowGreen.png";
import Slider from "react-slick";
import { Link } from "react-router-dom";
import i18next from "i18next";
import { getApi } from "../../utils/ServiceManager";
import Loader from "../../components/Loader";

const LatestNews = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    getData();
  }, []);
  const getData = () => {
    setIsLoading(true);
    getApi("news/home?limit=15", onSuccess, onFailure);
  };
  const onSuccess = (res) => {
    setIsLoading(false);
    if (res?.status) {
      setData(res?.data?.articles);
    } else {
      setError(res?.message);
    }
  };
  const onFailure = (err) => {
    setIsLoading(false);
    // setError(JSON.stringify(err?.message));
  };

  const NextArrow = (props) => {
    return (
      <div
        onClick={props.onClick}
        className={`justify-content-center align-items-center position-relative ${styles.nextIcon}`}
      >
        <img className="d-block" src={nextArrow} alt="First slide" />
      </div>
    );
  };
  const PrevArrow = (props) => {
    return (
      <div
        onClick={props.onClick}
        className={`justify-content-center align-items-center position-relative ${styles.prevIcon}`}
      >
        <img className="d-block" src={previousArrow} alt="First slide" />
      </div>
    );
  };
  const settings = {
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    initialSlide: 0,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    <>
      {/* <section className="container position-relative mb-5">
        <h1 className={`text-white-50 ${styles.title}`}>
          {i18next.language === "ar" ? "أخبار" : "News"}
        </h1>
        <div className="d-flex flex-column position-relative mt-5 gap-4">
          <div
            className={
              i18next.language === "ar" ? styles.imgOverlayAr : styles.imgOverlay
            }
          >
            <img
              className={`d-block shadow-md ${styles.featured}`}
              src={data[0]?.img_url}
              alt="Zahid-Lilani"
            />
            <div
              className={`d-flex justify-content-center align-items-center position-absolute rounded-circle flex-column ${styles.innerImage}`}
            >
              <div
                className={`d-flex justify-content-center align-items-center rounded-circle mt-2 ${styles.seeMore}`}
              >
                <p className="px-3 py-2">
                  <Link
                    to={`news-&-articles/${data[0]?.id}`}
                    state={{ id: data[0]?.id }}
                    className="text-black"
                  >
                    {i18next.t("SeeMore")}
                  </Link>
                </p>
              </div>
              <p className={styles.date}>{data[0]?.created_at}</p>
            </div>
          </div>
          <div className="d-flex flex-column container p-3 p-md-4 p-xl-5 shadow bg-white">
            {isLoading ? (
              <Loader />
            ) : (
              <>
                <h3 className={`h3 ${styles.aboutSubTitle}`}>{data[0]?.title}</h3>
                <p className={`p ${styles.aboutdesc}`}>{data[0]?.description}</p>
              </>
            )}
            <Link
              to={"/news-&-articles"}
              className={`d-flex align-items-center gap-2 mb-3 mb-lg-0 justify-content-end`}
            >
              <p className={`${styles.link} mb-0 fw-bold`}>
                {i18next.language === "ar" ? "مشاهدة الكل" : "View All"}
              </p>
              <div className="d-flex">
                <div className="w-auto mb-1">
                  <img className="d-block w-100" src={arrowForward} alt="" />
                </div>
                <div className="w-auto">
                  <img className="d-block w-100" src={arrowForward} alt="" />
                </div>
              </div>
            </Link>
            {isLoading ? (
              <Loader />
            ) : (
              <>
                {data?.length !== 0 && (
                  <div
                    className={`d-flex py-3 py-lg-4 py-xl-5 align-items-center mt-lg-4 mt-xl-5 ${styles.contactBox}`}
                  >
                    <Slider
                      className="container d-flex gap-md-2 gap-lg-4 align-items-end"
                      {...settings}
                    >
                      {data
                        .filter((x, index) => index !== 0)
                        ?.map((x, index) => {
                          return (
                            <Link
                              to={`/news-&-articles/${x?.id}`}
                              state={{ id: x?.id }}
                              key={x?.id}
                            >
                              <Card className="bg-transparent border-0 px-2">
                                <Card.Img
                                  variant="top"
                                  className={styles.imgCard}
                                  src={x?.img_url}
                                />
                                <Card.Body className={`p-0 pt-4`}>
                                  <h4
                                    className={`h5 text-white ${styles.titleCard}`}
                                  >
                                    {x?.title}
                                  </h4>
                                </Card.Body>
                              </Card>
                            </Link>
                          );
                        })}
                    </Slider>
                  </div>
                )}
              </>
            )}
          </div>
        </div>
      </section> */}


      <div className={`${styles.newsBackground}`}>
        <section className="container py-5">
          <h4 className={`text-center mb-5 pb-1 ${styles.newsTitle}`}>
            {i18next.language === "ar" ? "أخبار" : "News"}
          </h4>
          <div className="row">
            <div className="col-md-9">
              {isLoading ? (
                <Loader />
              ) : (
                <>
                  <h3 className={`${styles.aboutSubTitle}`}>{data[0]?.title}</h3>
                  <p className={`${styles.aboutdesc}`}>{data[0]?.description}</p>
                  <div className={`d-flex justify-content-center justify-content-md-end mt-5 ${styles.viewMoreButton}`}>

                    <Link
                      to={"/news-&-articles"}
                      className={``}
                    >

                      <p className={`m-0 ${styles.viewAllArticle}`}>
                        {i18next.language === "ar" ? "مشاهدة الكل" : "View All"}
                      </p>
                    </Link>
                  </div>
                </>
              )}
            </div>
            <div className="col-md-3 text-center text-md-start mt-5 mt-md-0">
              <img
                className={`${styles.featured}`}
                src={data[0]?.img_url}
                alt="Zahid-Lilani"
              />
            </div>
          </div>










          {/* <div
              className={
                i18next.language === "ar" ? styles.imgOverlayAr : styles.imgOverlay
              }
            >
              <img
                className={`${styles.featured}`}
                src={data[0]?.img_url}
                alt="Zahid-Lilani"
              />
              <div
                className={`d-flex justify-content-center align-items-center position-absolute rounded-circle flex-column ${styles.innerImage}`}
              >
                <div
                  className={`d-flex justify-content-center align-items-center rounded-circle mt-2 ${styles.seeMore}`}
                >
                  <p className="px-3 py-2">
                    <Link
                      to={`news-&-articles/${data[0]?.id}`}
                      state={{ id: data[0]?.id }}
                      className="text-black"
                    >
                      {i18next.t("SeeMore")}
                    </Link>
                  </p>
                </div>
                <p className={styles.date}>{data[0]?.created_at}</p>
              </div>
            </div> */}



          <div className="row">
            {/* {isLoading ? (
                <Loader />
              ) : (
                <>
                  <h3 className={`h3 ${styles.aboutSubTitle}`}>{data[0]?.title}</h3>
                  <p className={`p ${styles.aboutdesc}`}>{data[0]?.description}</p>
                </>
              )} */}
            {/* <Link
                to={"/news-&-articles"}
                className={`d-flex align-items-center gap-2 mb-3 mb-lg-0 justify-content-end`}
              >
                <p className={`${styles.link} mb-0 fw-bold`}>
                  {i18next.language === "ar" ? "مشاهدة الكل" : "View All"}
                </p>
                <div className="d-flex">
                  <div className="w-auto mb-1">
                    <img className="d-block w-100" src={arrowForward} alt="" />
                  </div>
                  <div className="w-auto">
                    <img className="d-block w-100" src={arrowForward} alt="" />
                  </div>
                </div>
              </Link> */}




            {isLoading ? (
              <Loader />
            ) : (
              <>
                {data?.length !== 0 && (
                  <div
                    className={`py-3 py-lg-4 py-xl-5 align-items-center mt-5 ${styles.contactBox}`}
                  >
                    <Slider
                      className="container d-flex gap-md-2 gap-lg-4 align-items-end position-relative px-4"
                      {...settings}
                    >
                      {data
                        .filter((x, index) => index !== 0)
                        ?.map((x, index) => {
                          return (
                            <Link
                              to={`/news-&-articles/${x?.id}`}
                              state={{ id: x?.id }}
                              key={x?.id}
                            >
                              <Card className="bg-transparent border-0 px-2">
                                <div className={`${i18next.language === "ar"
                                  ? styles.imageSlideContentAr
                                  : styles.imageSlideContent
                                  }`}>
                                  <Card.Img
                                    variant="top"
                                    className={styles.imgCard}
                                    src={x?.img_url}
                                  />
                                  <Card.Body className={`p-2 pt-0`}>
                                    <h4
                                      className={`h5 ${styles.titleCard}`}
                                    >
                                      {x?.title}
                                    </h4>
                                  </Card.Body>
                                </div>
                              </Card>
                            </Link>
                          );
                        })}
                    </Slider>
                  </div>
                )}
              </>
            )}
          </div>

        </section>
      </div>


    </>
  );
};

export default LatestNews;
