import React, { useEffect, useState } from "react";
import styles from "./issueDetails.module.scss";
import separator from "../../assets/images/separator.png";
import greenBgIssue from "../../assets/images/greenBgIssue.png";
import fileDoc from "../../assets/images/fileDoc.png";
import user from "../../assets/images/user.png";
import calendar from "../../assets/images/calendar.png";
import { useParams } from "react-router-dom";
import { getApi } from "../../utils/ServiceManager";
import Loader from "../../components/Loader";
import starBg from "../../assets/images/darkStarBg.png";
import i18next from "i18next";
import fileDownload from "js-file-download";
import axios from "axios";

const IssueDetails = () => {
  const { id, name } = useParams();
  const [data, setData] = useState([]);
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    getData();
  }, []);
  const getData = () => {
    setIsLoading(true);
    getApi(`issues/article?id=${id}`, onSuccess, onFailure);
  };

  const onSuccess = (res) => {
    setIsLoading(false);
    if (res?.status) {
      setData(res?.data?.article);
    } else {
      setError(res?.message);
    }
  };
  const onFailure = (err) => {
    setIsLoading(false);
    // setError(JSON.stringify(err?.message));
  };
  const download = () => {
    const instance = axios.create({
      baseURL: `${process.env.REACT_APP_PROD_API_URL}/${
        i18next.language === "ar" ? "ar" : "en"
      }`,
    });
    instance.defaults.headers.common["Authorization"] = "Bearer password";
    instance
      .get(data?.download_link, { responseType: "blob" })
      .then((response) => {
        fileDownload(response?.data, `${data?.title}.pdf`);
      })
      .catch((error) => {
        onFailure(error);
      });
  };
  return (
    <>
      <img
        className={`d-block w-50 position-absolute ${
          i18next.language === "ar" ? styles.imgAr : styles.img
        }`}
        src={starBg}
        alt="First slide"
      />
      <section
        className="container position-relative mb-5"
        style={{ zIndex: 10 }}
      >
        {isLoading ? (
          <Loader />
        ) : (
          <>
            <h2
              className={`mt-4 mt-md-5 mx-auto text-center ${styles.subTitlePage}`}
            >
              {data?.title}
            </h2>
            <h1 className={`mb-4 mb-md-5 mt-3 mx-auto ${styles.title}`}>
              {name}
            </h1>
            <img
              className="d-block w-100"
              style={{ height: "1rem" }}
              src={separator}
              alt="Separator"
            />
            <div
              className={`d-flex flex-column flex-lg-row container p-3 p-md-4 p-xl-5 shadow rounded bg-white ${styles.boxBg}`}
            >
              <div className="col-12 col-lg-7">
                <h3 className={`fw-bold ${styles.issueTitle}`}>
                  {data?.title}
                </h3>
                <div
                  className={`d-flex flex-column flex-md-row gap-3 gap-md-5 my-3`}
                >
                  <div className="d-flex align-items-center gap-2">
                    <div>
                      <img className="d-block w-100" src={user} alt="user" />
                    </div>
                    <p className="m-0 text-black">{data?.author}</p>
                  </div>
                  <div className="d-flex align-items-center gap-2">
                    <div>
                      <img
                        className="d-block w-100"
                        src={calendar}
                        alt="calendar"
                      />
                    </div>
                    <p className="m-0 text-black">{data?.created_at}</p>
                  </div>
                </div>
                <div className="d-flex flex-column flex-md-row gap-3 gap-lg-1 gap-xxl-4 align-items-md-center my-3">
                  <p className={`fw-bold text-black m-0 ${styles.footText}`}>
                    VIEWS :{" "}
                    <span className={styles.gold}>{data?.read_count}</span>
                  </p>
                  <p className={`fw-bold text-black m-0 ${styles.footText}`}>
                    Pdf-Downloads :{" "}
                    <span className={styles.gold}>{data?.download_count}</span>
                  </p>
                </div>
                <p className={styles.aboutdesc}>{data?.description}</p>
              </div>
              <div
                className="col-12 col-lg-5 d-flex flex-column align-items-center position-relative"
                style={{ height: "fit-content" }}
              >
                <img
                  src={greenBgIssue}
                  alt=""
                  className={`d-block w-100 ${styles.bgImage}`}
                />
                <img
                  src={data?.issue_cover_img ? data?.issue_cover_img : fileDoc}
                  alt=""
                  className={`d-block position-absolute ${styles.imgOverlay}`}
                />
                <button
                  onClick={download}
                  className={`border-0 bg-transparent ${styles.linkButton}`}
                >
                  <span
                    className={`badge rounded-pill w-100 py-3 fs-5 ${styles.pill}`}
                  >
                    {i18next.language === "ar"
                      ? "تحميل الملف"
                      : "Download File"}
                  </span>
                </button>
              </div>
            </div>
          </>
        )}
        {/* {error && <h2 className="text-danger text-center">{error}</h2>} */}
      </section>
    </>
  );
};

export default IssueDetails;
