import React, { useEffect, useState } from "react";
import styles from "./search.module.scss";
import separator from "../../assets/images/separator.png";
import { Form } from "react-bootstrap";
import search from "../../assets/images/search.png";
import { Link, useLocation } from "react-router-dom";
import user from "../../assets/images/user.png";
import calendar from "../../assets/images/calendar.png";
import { getApi } from "../../utils/ServiceManager";
import i18next from "i18next";
import { useFormik } from "formik";
import { Translation } from "react-i18next";
import Loader from "../../components/Loader";
import fileDownload from "js-file-download";
import axios from "axios";

const validate = (values) => {
  const errors = {};
  if (!values.query) {
    errors.query =
      i18next.language === "ar"
        ? "مطلوب اللقب أو الاسم"
        : "Title or Name Required";
  }
  return errors;
};
const Search = () => {
  const location = useLocation();
  const [data, setData] = useState([]);
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    location?.state?.query && getData(location?.state?.query);
  }, []);
  const getData = (query) => {
    setIsLoading(true);
    getApi(`issues/search?search=${query}`, onSuccess, onFailure);
  };
  const onSuccess = (res) => {
    setIsLoading(false);
    if (res?.status) {
      if (res?.data?.articles?.length === 0) {
        setData([]);
        setError(i18next.language === "ar" ? "لاتوجد بيانات" : "No data found");
      } else {
        setData(res?.data?.articles);
        setError();
      }
    } else {
      setError(res?.message);
    }
  };
  const onFailure = (err) => {
    setIsLoading(false);
    // setError(JSON.stringify(err?.message));
  };
  const formik = useFormik({
    initialValues: {
      query: location?.state?.query || "",
    },
    validate,
    onSubmit: (values) => {
      getData(values?.query);
    },
  });
  const download = (link, title) => {
    const instance = axios.create({
      baseURL: `${process.env.REACT_APP_PROD_API_URL}/${
        i18next.language === "ar" ? "ar" : "en"
      }`,
    });
    instance.defaults.headers.common["Authorization"] = "Bearer password";
    instance
      .get(link, { responseType: "blob" })
      .then((response) => {
        fileDownload(response?.data, `${title}.pdf`);
      })
      .catch((error) => {
        onFailure(error);
      });
  };
  return (
    <Translation>
      {(t) => (
        <section className="container">
          <h1
            className={`my-4 my-md-5 mx-auto text-center ${styles.pageTitle}`}
          >
            {t("Header.Search")}
          </h1>
          <Form
            noValidate
            className="d-flex justify-content-center my-4 my-md-5"
          >
            <div
              className={`input-group rounded-3 px-2 px-md-3 ${styles.searchTextInput}`}
            >
              <input
                id="query"
                name="query"
                value={formik.values.query}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                type="text"
                className={`form-control bg-transparent border-0 ${styles.placeholder}`}
                placeholder={t("Form.SearchHere")}
                aria-label="Search here"
                aria-describedby="button-addon2"
              />
              <button
                type={"submit"}
                onClick={formik.handleSubmit}
                className={`btn ${styles.regularButton}`}
              >
                <img src={search} alt="" />
              </button>
            </div>
          </Form>
          {formik.errors.query && (
            <p className="text-danger mt-2 text-center">
              {formik.errors.query}
            </p>
          )}
          <img
            className="d-block w-100"
            style={{ height: "1rem" }}
            src={separator}
            alt="Separator"
          />
          <div className="d-flex flex-wrap justify-content-between my-4">
            {isLoading ? (
              <Loader />
            ) : (
              data?.length !== 0 &&
              data?.map((x) => {
                return (
                  <div
                    key={x?.id}
                    className={`d-flex flex-column justify-content-center align-items-center my-4 p-3 p-md-4 rounded-3 ${styles.box}`}
                  >
                    <Link
                      to={`/archive/${x?.issue}/${x?.id}/${x?.title
                        ?.toLowerCase()
                        .replace(/ /g, "-")}`}
                    >
                      <h2 className={`h5 fw-bold ${styles.title}`}>
                        {x?.title}
                      </h2>
                    </Link>
                    <div className={`w-100 py-3 ${styles.boxDesc}`}>
                      <div
                        className={`d-flex flex-column flex-md-row gap-3 gap-md-5 mb-3`}
                      >
                        <div className="d-flex align-items-center gap-2">
                          <div>
                            <img
                              className="d-block w-100"
                              src={user}
                              alt="user"
                            />
                          </div>
                          <p className={`m-0 text-black ${styles.author}`}>
                            {x?.author}
                          </p>
                        </div>
                        <div className="d-flex align-items-center gap-2">
                          <div>
                            <img
                              className="d-block w-100"
                              src={calendar}
                              alt="calendar"
                            />
                          </div>
                          <p className="m-0 text-black">{x?.created_at}</p>
                        </div>
                      </div>
                      <p className={`text-black mb-3 ${styles.pLink}`}>
                        <a href={x?.doi_link}>{x?.doi_link}</a> -{" "}
                        {i18next.language === "ar"
                          ? "مجلة الوقف"
                          : "Al Wagt Journal"}{" "}
                        - {x?.page_no}
                      </p>
                      <p className={`text-black mb-0 ${styles.pLink}`}>
                        This work and the related PDF file are licensed under a
                        <a href="https://creativecommons.org/licenses/by/4.0/">
                          {" "}
                          Creative Commons Attribution 4.0 International
                        </a>
                      </p>
                    </div>
                    <div className={`d-flex w-100 ${styles.boxDesc}`}>
                      <div className="d-flex flex-column flex-md-row w-100 mt-3 justify-content-between gap-2 gap-md-0">
                        <div className="d-flex gap-3 gap-lg-1 gap-xxl-3 align-items-center">
                          <p
                            className={`fw-bold text-black m-0 ${styles.footText}`}
                          >
                            VIEWS :{" "}
                            <span className={styles.gold}>{x?.read_count}</span>
                          </p>
                          <p
                            className={`fw-bold text-black m-0 ${styles.footText}`}
                          >
                            Pdf-Downloads :{" "}
                            <span className={styles.gold}>
                              {x?.download_count}
                            </span>
                          </p>
                        </div>
                        <div className="d-flex gap-3">
                          <a>
                            <span
                              onClick={() =>
                                download(x?.download_link, x?.title)
                              }
                              className={`badge rounded-pill px-4 px-lg-2 px-xxl-4 fs-6 d-flex align-items-center justify-content-center ${styles.pill}`}
                            >
                              Full Text.Pdf
                            </span>
                          </a>
                          <Link
                            to={`/archive/${x?.issue}/${x?.id}/${x?.title
                              ?.toLowerCase()
                              .replace(/ /g, "-")}`}
                          >
                            <span
                              className={`badge rounded-pill px-4 px-lg-2 px-xxl-4 fs-6 d-flex align-items-center justify-content-center ${styles.pill}`}
                            >
                              Html
                            </span>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })
            )}
          </div>
          {error && <h2 className="text-danger text-center">{error}</h2>}
        </section>
      )}
    </Translation>
  );
};

export default Search;
