import React, { useState } from "react";
import PageHeader from "../../components/PageHeader";
import styles from "./help.module.scss";
import { Translation } from "react-i18next";
import i18next from "i18next";
import { useFormik } from "formik";
import { postApi } from "../../utils/ServiceManager";
import Button from "../../components/Button";
import { Form } from "react-bootstrap";

const validate = (values) => {
  const errors = {};
  if (!values.name) {
    errors.name = i18next.language === "ar" ? "الاسم مطلوب" : "Name Required";
  } else if (values.name.length > 15) {
    errors.name =
      i18next.language === "ar"
        ? "يجب أن يكون عدد الأحرف 15 حرفًا أو أقل"
        : "Must be 15 characters or less";
  }
  if (!values.email) {
    errors.email =
      i18next.language === "ar" ? "البريد الإلكتروني (مطلوب" : "Email Required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email =
      i18next.language === "ar"
        ? "عنوان البريد الإلكتروني غير صالح"
        : "Invalid email address";
  }
  if (!values.subject) {
    errors.subject =
      i18next.language === "ar" ? "الموضوع (مطلوب" : "Subject Required";
  }
  if (!values.message) {
    errors.message =
      i18next.language === "ar" ? "الرسالة المطلوبة" : "Message Required";
  }
  return errors;
};
const Help = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();
  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      subject: "",
      message: "",
    },
    validate,
    onSubmit: (values, { resetForm }) => {
      setIsLoading(true);
      postApi(
        "contact-us",
        values,
        (res) => {
          setIsLoading(false);
          setError(res?.message);
          if (res?.status) {
            resetForm({ values: "" });
          }
        },
        (err) => {
          setIsLoading(false);
          setError(err?.message.toString());
        }
      );
    },
  });
  return (
    <Translation>
      {(t) => (
        <>
          <section className="container">
            <PageHeader pageTitle={t("Header.help")} />
            <div className="my-5 d-flex justify-content-center">
              <div
                className={`bg-white bg-opacity-10 p-3 p-md-5 rounded-3 d-flex flex-column ${styles.box}`}
              >
                <Form noValidate>
                  <Form.Group className="mb-3">
                    <Form.Label className={styles.label}>
                      {t("Form.YourName")}
                    </Form.Label>
                    <Form.Control
                      id="name"
                      name="name"
                      value={formik.values.name}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      className={`rounded-3 bg-transparent ${styles.input}`}
                      placeholder={t("Form.EnterName")}
                    />
                    <Form.Text className="text-danger">
                      {formik.errors.name}
                    </Form.Text>
                  </Form.Group>
                  <Form.Group className="mb-3">
                    <Form.Label className={styles.label}>
                      {t("Form.ContactEmail")}
                    </Form.Label>
                    <Form.Control
                      id="email"
                      name="email"
                      value={formik.values.email}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      className={`rounded-3 bg-transparent w-50 ${styles.input}`}
                      type="email"
                      placeholder={t("Form.EnterEmail")}
                    />
                    <Form.Text className="text-danger">
                      {formik.errors.email}
                    </Form.Text>
                  </Form.Group>
                  <Form.Group className="mb-3">
                    <Form.Label className={styles.label}>
                      {t("Form.Subject")}
                    </Form.Label>
                    <Form.Control
                      id="subject"
                      name="subject"
                      value={formik.values.subject}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      className={`rounded-3 bg-transparent ${styles.input}`}
                      placeholder={t("Form.EnterSubject")}
                    />
                    <Form.Text className="text-danger">
                      {formik.errors.subject}
                    </Form.Text>
                  </Form.Group>
                  <Form.Group className="mb-3">
                    <Form.Label className={styles.label}>
                      {t("Form.YourMessage")}
                    </Form.Label>
                    <Form.Control
                      id="message"
                      name="message"
                      value={formik.values.message}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      className={`rounded-3 bg-transparent ${styles.input}`}
                      as="textarea"
                      style={{ height: "100px" }}
                      placeholder={t("Form.EnterMessage")}
                    />
                    <Form.Text className="text-danger">
                      {formik.errors.message}
                    </Form.Text>
                  </Form.Group>

                  <Button
                    disabled={isLoading}
                    onClick={formik.handleSubmit}
                    type="submit"
                    className={`text-white w-100 mt-2 mt-md-4 ${styles.btnSubmit}`}
                    text={
                      isLoading ? (
                        <>
                          <span
                            className="spinner-border spinner-border-sm mx-3"
                            role="status"
                            aria-hidden="true"
                          ></span>
                          {i18next.language === "ar"
                            ? "جار التحميل..."
                            : "Loading..."}
                        </>
                      ) : (
                        t("Submit")
                      )
                    }
                  />
                </Form>
                {error && (
                  <h2 className="text-black text-center mt-3">{error}</h2>
                )}
              </div>
            </div>
          </section>
        </>
      )}
    </Translation>
  );
};

export default Help;
