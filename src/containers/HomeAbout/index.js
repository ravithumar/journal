import React, { useEffect, useState } from "react";
import textAr1 from "../../assets/images/textAr1.png";
import textAr2 from "../../assets/images/textAr2.png";
import zahidLilani from "../../assets/images/testImage.jpg";
import docResearch from "../../assets/images/docResearch.png";
import styles from "./homeAbout.module.scss";
import Button from "../../components/Button";
import { Link, useNavigate } from "react-router-dom";
import { getApi } from "../../utils/ServiceManager";
import { useTranslation } from "react-i18next";
import i18next from "i18next";
import Loader from "../../components/Loader";
import greyBox from '../../assets/images/grey_box.svg';
import crossRef from '../../assets/images/crossref.svg';
import screenedBY from '../../assets/images/screenedBy.svg';
const HomeAbout = () => {
  const { i18n } = useTranslation();
  const navigate = useNavigate();
  const [data, setData] = useState({});
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    getData();
  }, []);
  const getData = () => {
    setIsLoading(true);
    getApi("about", onSuccess, onFailure);
  };
  const onSuccess = (res) => {
    setIsLoading(false);
    if (res?.status) {
      setData(res?.data);
    } else {
      setError(res?.message);
    }
  };
  const onFailure = (err) => {
    setIsLoading(false);
    // setError(JSON.stringify(err?.message));
  };
  return (
    <>
      {/* <section
        className="container position-relative mb-5"
        style={{ marginTop: "4rem" }}
      >
        <div className={`d-flex justify-content-around ${styles.imgArabic}`}>
          <div>
            <img className="d-block w-100" src={textAr1} alt="Text Arabic" />
          </div>
          <div>
            <img className="d-block w-100" src={textAr2} alt="Text Arabic" />
          </div>
        </div>
        {isLoading ? (
          <Loader />
        ) : (
          data !== {} && (
            <div
              className="d-flex flex-column position-relative"
              style={{
                gap: "3rem",
                marginTop: "5rem",
              }}
            >
              <div className="col-md-6 d-flex flex-column justify-content-center gap-3">
                <h2 className={`h2 ${styles.aboutTitle}`}>
                  {i18n.t("About.Bait")}
                </h2>
                <div>
                  <p className={`p ${styles.carouselPara}`}>
                    {i18n.t("About.desc1")}
                  </p>
                  <p className={`p ${styles.carouselPara}`}>
                    {i18n.t("About.desc2")}
                  </p>
                  <p className={`p ${styles.carouselPara}`}>
                    {i18n.t("About.desc3")}
                  </p>
                </div>
              </div>
              <div
                className={
                  i18next.language === "ar"
                    ? styles.imgOverlayAr
                    : styles.imgOverlay
                }
              >
                <img
                  className="d-block w-100"
                  src={zahidLilani}
                  alt="Zahid-Lilani"
                  style={{ zIndex: 1 }}
                />
                <div
                  className={`d-flex justify-content-center align-items-center position-absolute rounded-circle flex-column ${styles.innerImage}`}
                >
                  <Link
                    to={"/journal"}
                    className={`d-flex justify-content-center align-items-center rounded-circle mt-2 ${styles.seeMore}`}
                  >
                    <p className="p px-3 py-2 text-black">{i18n.t("SeeMore")}</p>
                  </Link>
                  <p
                    style={{
                      flex: "1",
                      marginBottom: 0,
                      justifyContent: "inherit",
                      display: "flex",
                      flexDirection: "column",
                    }}
                  >
                  </p>
                </div>
              </div>
              <div
                className="d-flex flex-column container p-3 p-md-4 p-xl-5 shadow rounded"
                style={{
                  backgroundColor: "white",
                }}
              >
                <h3 className={`h3 ${styles.aboutSubTitle}`}>
                  {i18n.t("About.About")}
                </h3>
                <p className={`p ${styles.aboutdesc}`}>{data?.about}</p>
                <h3 className={`h3 ${styles.aboutSubTitle}`}>
                  {i18n.t("About.Vision")}
                </h3>
                <p className={`p ${styles.aboutdesc}`}>{data?.vision}</p>
                <h3 className={`h3 ${styles.aboutSubTitle}`}>
                  {i18n.t("About.Message")}
                </h3>
                <p className={`p ${styles.aboutdesc}`}>{data?.message}</p>
                <div
                  className={`d-flex flex-column flex-md-row flex-wrap p-3 p-lg-4 p-xl-5 align-items-center gap-2 gap-md-3 gap-xl-5 mt-lg-4 mt-xl-5 ${styles.contactBox}`}
                >
                  <div
                    className={`d-flex justify-content-center ${styles.imgContact}`}
                  >
                    <img
                      className="d-block w-75"
                      src={docResearch}
                      alt="Research Submit"
                    />
                  </div>
                  <div className={`${styles.contactTitleDiv} p-3`}>
                    <h4 className={`h4 fs-2 ${styles.contactTitle}`}>
                      {i18n.t("Home.ResearchTitle")}
                    </h4>
                    <p className={`p ${styles.contactDesc}`}>
                      {i18n.t("Home.ResearchDesc")}
                    </p>
                  </div>
                  <Button
                    onClick={() => navigate("/research-form")}
                    text={i18n.t("SubmitHere")}
                    className={`${styles.contactButton} mt-4`}
                  />
                </div>
              </div>
            </div>
          )
        )}
      </section> */}



















      <div className={`${styles.aboutTopBackground} py-5`}>
        {isLoading ? (
          <Loader />
        ) : (
          data !== {} && (
            <section
              className="container"
            >

              <div className="row">
                <div className="col-md-8">
                  <h3 className={`h3 ${styles.aboutSubTitle}`}>
                    {i18n.t("About.About")}
                  </h3>
                  <p className={`p ${styles.aboutdesc}`}>{data?.about}</p>
                  <h3 className={`h3 ${styles.aboutSubTitle}`}>
                    {i18n.t("About.Vision")}
                  </h3>
                  <p className={`p ${styles.aboutdesc}`}>{data?.vision}</p>
                  <h3 className={`h3 ${styles.aboutSubTitle}`}>
                    {i18n.t("About.Message")}
                  </h3>
                  <p className={`p ${styles.aboutdesc}`}>{data?.message}</p>
                </div>


                <div className={`col-md-4 ${i18next.language === "ar"
                  ? 'text-end text-md-start'
                  : 'text-start text-md-end'
                  }`}>
                  <div className={
                    i18next.language === "ar"
                      ? styles.imgOverlayAr
                      : styles.imgOverlay
                  }
                  >
                    <img
                      className={`${styles.zahidImage}`}
                      src={zahidLilani}
                      alt="Zahid-Lilani"
                    />

                  </div>
                  <div className="mt-4">
                    <Link
                      to={"/journal"}
                      className={`${styles.seeMore}`}
                    >
                      {i18n.t("SeeMore")}
                    </Link>
                  </div>
                </div>


              </div>
            </section>
          )
        )}
      </div>


      <div className={`${styles.aboutBackground}`}>
        <section
          className="container"
        >
          <div
            className={`d-flex justify-content-between flex-column flex-md-row flex-wrap align-items-center gap-2 gap-md-3 gap-xl-5 ${styles.contactBox}`}
          >
            <div className={`${styles.contactTitleDiv} py-3`}>
              <h4 className={`h4 fs-2 ${styles.contactTitle}`}>
                {i18n.t("Home.ResearchTitle")}
              </h4>
              <p className={`p ${styles.contactDesc}`}>
                {i18n.t("Home.ResearchDesc")}
              </p>
              <div className={`text-center text-md-start ${styles.seemoreButton}`}>
                <Button
                  onClick={() => navigate("/research-form")}
                  text={i18n.t("SubmitHere")}
                  className={`${styles.contactButton} mt-4`}
                />
              </div>
            </div>
            <div
              className={`position-relative ${styles.imgContact}`}
            >
              <img
                className="imgBoxBack"
                src={greyBox}
                alt="Research Submit"
              />
              <div className={`position-absolute ${styles.insideImgBox}`}>
                <img
                  className="imgBoxBack2"
                  src={crossRef}
                  alt="Research Submit"
                />
                <img
                  className="mt-2"
                  src={screenedBY}
                  alt="Research Submit"
                />
              </div>

              {/* <p>sddssd</p> */}
              {/* <img
                className=""
                src={docResearch}
                alt="Research Submit"
              /> */}
            </div>

          </div>
        </section>
      </div>
    </>
  );
};

export default HomeAbout;
