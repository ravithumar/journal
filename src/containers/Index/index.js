import React, { useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import PageHeader from "../../components/PageHeader";
import { getApi } from "../../utils/ServiceManager";
import { Translation } from "react-i18next";
import Loader from "../../components/Loader";
import styles from "./index.module.scss";
const Index = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    getData();
  }, []);
  const getData = () => {
    setIsLoading(true);
    getApi("indices/list", onSuccess, onFailure);
  };
  const onSuccess = (res) => {
    setIsLoading(false);
    if (res?.status) {
      setData(res?.data?.indices);
    } else {
      setError(res?.message);
    }
  };
  const onFailure = (err) => {
    setIsLoading(false);
    // setError(JSON.stringify(err?.message));
  };
  return (
    <Translation>
      {(t) => (
        <section className="container mb-5">
          <PageHeader pageTitle={t("Header.index&Database")} />
          <div className="d-flex flex-wrap justify-content-between gap-2 mt-4 pb-4">
            {isLoading ? (
              <Loader />
            ) : (
              <>
                {data.length !== 0 &&
                  data?.map((x, index) => {
                    return (
                      <a
                        target={"_blank"}
                        rel="noopener noreferrer"
                        href={x?.redirect_url}
                        key={index}
                        className={`col-12 col-md-5 col-xl-3 bg-white d-flex flex-column justify-content-center align-items-center my-4 ${styles.companies}`}
                      >
                        <Card.Img
                          variant="top"
                          src={x?.img_url}
                          className="mx-auto py-4 w-auto"
                        />
                        <Card.Body className="d-flex justify-content-center border-top w-75">
                          <h2 className="h4 text-black">{x?.name}</h2>
                        </Card.Body>
                      </a>
                    );
                  })}
              </>
            )}
          </div>
          {/* {error && <h2 className="text-danger text-center">{error}</h2>} */}
        </section>
      )}
    </Translation>
  );
};

export default Index;
