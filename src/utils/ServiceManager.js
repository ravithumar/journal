import axios from "axios";
import i18next from "i18next";

export const getApi = async (url, onSuccess, onFailure) => {
  const instance = axios.create({
    baseURL: `${process.env.REACT_APP_PROD_API_URL}/${
      i18next.language === "ar" ? "ar" : "en"
    }`,
  });
  instance.defaults.headers.common["Authorization"] = "Bearer password";
  instance
    .get(url)
    .then((response) => {
      onSuccess(response.data);
    })
    .catch((error) => {
      onFailure(error);
    });
};
export const postApi = async (url, params, onSuccess, onFailure) => {
  const instance = axios.create({
    baseURL: `${process.env.REACT_APP_PROD_API_URL}/${
      i18next.language === "ar" ? "ar" : "en"
    }`,
  });
  instance.defaults.headers.common["Authorization"] = "Bearer password";
  instance
    .post(url, params)
    .then((response) => {
      onSuccess(response.data);
    })
    .catch((error) => {
      onFailure(error);
    });
};
