export const defaultLanguage = "ar";

export const supportedLanguages = [
  { code: "ar", name: "عربي" },
  { code: "en", name: "English" }
];
