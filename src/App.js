import React, { lazy, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.scss";
import ScrollToTop from "./components/ScrollToTop";
import LocaleContext from "./LocaleContext";
import Sidebar from "./components/Sidebar";
const Header = lazy(() => import("./components/Header"));
const Footer = lazy(() => import("./components/Footer"));
const Home = lazy(() => import("./containers/Home"));
const Journal = lazy(() => import("./containers/Journal"));
const EditorialBoard = lazy(() => import("./containers/EditorialBoard"));
const AdvisoryBoard = lazy(() => import("./containers/AdvisoryBoard"));
const Author = lazy(() => import("./containers/Author"));
const PublicationEthics = lazy(() => import("./containers/PublicationEthics"));
const Index = lazy(() => import("./containers/Index"));
const Archive = lazy(() => import("./containers/Archive"));
const YearIssues = lazy(() => import("./containers/YearIssues"));
const IssueDetails = lazy(() => import("./containers/IssueDetails"));
const Faqs = lazy(() => import("./containers/Faqs"));
const Help = lazy(() => import("./containers/Help"));
const Contact = lazy(() => import("./containers/Contact"));
const ResearchForm = lazy(() => import("./containers/ResearchForm"));
const TermsConditions = lazy(() => import("./containers/TermsConditions"));
const Search = lazy(() => import("./containers/Search"));
const NewsArticles = lazy(() => import("./containers/NewsArticles"));
const NewsDetails = lazy(() => import("./containers/NewsDetails"));
const Privacy = lazy(() => import("./containers/Privacy"));
const PageNotFound = lazy(() => import("./containers/PageNotFound"));

function App() {
  const [isOpened, setIsOpened] = useState(false);
  const { i18n } = useTranslation();
  const [locale, setLocale] = useState(i18n.language);
  useEffect(() => {
// console.log(locale);
    // var userLang = window.navigator.language;
    // console.log(userLang);
    const check = localStorage.getItem('i18nextLng');
    if (check == 'en') {
      i18n.changeLanguage('en');
    } else {
      i18n.changeLanguage('ar');
    }

    i18n.on("languageChanged", (lng) => setLocale(lng));
    document.dir = i18n.dir();
    document.documentElement.lang = locale;
    document.title = i18n.t("document_title");

  }, [i18n, locale]);


  // useEffect(() => {
  //   var userLang = window.navigator.language;
  //   console.log(userLang);
  //   if (userLang === 'en-IN' || userLang === 'en' || userLang === 'en-US') {
  //     i18n.changeLanguage("ar");
  //   }
  // },[])
  return (
    <LocaleContext.Provider value={{ locale, setLocale }}>
      <BrowserRouter>
        <ScrollToTop>
          <Header toggleMenu={setIsOpened} />
          <Sidebar show={isOpened} setIsOpened={setIsOpened} />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/journal" element={<Journal />} />
            <Route path="/editorial-board" element={<EditorialBoard />} />
            <Route path="/advisory-board" element={<AdvisoryBoard />} />
            <Route path="/author" element={<Author />} />
            <Route path="/publication-ethics" element={<PublicationEthics />} />
            <Route path="/index-&-database" element={<Index />} />
            <Route path="/archive" element={<Archive />} />
            <Route path="/archive/:name" element={<YearIssues />} />
            <Route path="/archive/:name/:id/:slug" element={<IssueDetails />} />
            <Route path="/faqs" element={<Faqs />} />
            <Route path="/help" element={<Help />} />
            <Route path="/contact-us" element={<Contact />} />
            <Route path="/research-form" element={<ResearchForm />} />
            <Route path="/terms-&-conditions" element={<TermsConditions />} />
            <Route path="/privacy-policy" element={<Privacy />} />
            <Route path="/search" element={<Search />} />
            <Route path="/news-&-articles" element={<NewsArticles />} />
            <Route path="/news-&-articles/:id" element={<NewsDetails />} />
            <Route path="*" element={<PageNotFound />} />
          </Routes>
          <Footer />
        </ScrollToTop>
      </BrowserRouter>
    </LocaleContext.Provider>
  );
}

export default App;
