import React, { useEffect, useState } from "react";
import styled from "styled-components";
import styles from "./modal.module.scss";
import { Translation, useTranslation } from "react-i18next";
import waqfaq from "../../assets/images/WAQFQA.svg";
import arFooter2 from "../../assets/images/newFooterLogo.svg";
import footLogo from "../../assets/images/footLogo.svg";
import facebook from "../../assets/images/Facebook_sidebar.svg";
import instagram from "../../assets/images/Instagram_sidebar.svg";
import twitter from "../../assets/images/Twiteer_sidebar.svg";
import youtube from "../../assets/images/Youtube_sidebar.svg";
import logosidebar from "../../assets/images/Logo_sidebar.svg";
import cross from "../../assets/images/Cross_sidebar.svg";
import { getApi } from "../../utils/ServiceManager";
import Loader from "../Loader";

const SidebarStyled = styled.div`
  position: fixed;
  z-index: 555;
  top: 0;
  width: 100%;
  background-color: #fff;
  color: #fff;
  max-width: 400px;
  height: 100%;
  transition: all 0.3s ease-in-out;
`;

const Link = styled.a`
  text-decoration: none;
  color: #006548;
  // font-family: inherit;
  padding: 1em 2rem;
  font-size: 20px;

  &:first-of-type {
    // margin-top: 50px;
  }
  &:hover{
    color:#006548 !important;
  }
  &:visited{
    color:#006548 !important;
  }
`;

const Sidebar = ({ show, setIsOpened }) => {
  const { i18n } = useTranslation();
  const [locale, setLocale] = useState(i18n.language);
  const [data, setData] = useState([]);
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);

  console.log(locale.includes("en"), ' <<< LOCALE')
  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    setIsLoading(true);
    getApi("services/list", onSuccess, onFailure);
  };

  const onSuccess = (res) => {
    setIsLoading(false);
    if (res?.status) {
      setData(res?.data?.services);
    } else {
      setError(res?.message);
    }
  };

  const onFailure = (err) => {
    setIsLoading(false);
  };

  return (
    <Translation>
      {(t) => (
        <SidebarStyled className={show ? locale.includes("en") ? styles.ltr_show : styles.rtl_show : locale.includes("en") ? styles.ltr : styles.rtl} show={show ? 1 : 0}>
          <div
            onClick={() => {
              setIsOpened(false);
              console.log("Close icon clicked, close sidebar");
            }}
            className={`${styles.crossStyle}`}
          >
            <img
              className="mx-1"
              src={cross}
              alt="cross"
            />

          </div>
          <div className="d-flex flex-column justify-content-between h-100">
            <div className={`d-flex p-5 justify-content-center ${styles.logoSec}`}>
              <div>
                <img
                  className=""
                  src={logosidebar}
                  alt="footerLogo"
                />
              </div>
            </div>
            <div className="d-flex flex-column overflow-auto">
              {/* <Link href="https://www.awqaf.gov.qa/tenders" target={"_blank"}>{t("ServiceHeader.tenders")}</Link>
              <Link href="https://www.awqaf.gov.qa/forms" target={"_blank"}>{t("ServiceHeader.formsdocuments")}</Link>
              <Link href="https://www.awqaf.gov.qa/projects" target={"_blank"}>{t("ServiceHeader.ourprojects")}</Link>
              <Link href="https://b-mashura.com/en/services/financial-consultations/" target={"_blank"}>{t("ServiceHeader.financialconsultation")}</Link>
              <Link href="https://b-mashura.com/en/services/translation-services/" target={"_blank"}>{t("ServiceHeader.translationservice")}</Link> */}
              {isLoading ? (
                <Loader />
              ) : (
                <>
                  {data.length !== 0 &&
                    data?.map((x, index) => {
                      return (
                        <Link href={x.redirect_url} target={"_blank"}>{x.name}</Link>
                      );
                    })}
                </>
              )}
            </div>
            <div style={{ background: '#7fbe5c', width: '100%', padding: "20px", textAlign: 'center', zIndex: '555' }}>
              <div className="mx-3">
                <a
                  className=""
                  target={"_blank"}
                  rel="noopener noreferrer"
                  href="https://www.facebook.com/mashurajournal.qa/"
                >
                  <img
                    className="mx-2"
                    src={facebook}
                    alt="facebookIcon"
                  />
                </a>
                <a
                  className="w-100"
                  target={"_blank"}
                  rel="noopener noreferrer"
                  href="https://www.instagram.com/bait_almashura/"
                >
                  <img
                    className="mx-2"
                    src={instagram}
                    alt="instagramIcon"
                  />
                </a>
                <a
                  className="w-100"
                  target={"_blank"}
                  rel="noopener noreferrer"
                  href="https://twitter.com/baitalmashura"
                >
                  <img
                    className="mx-2"
                    src={twitter}
                    alt="twitterIcon"
                  />
                </a>
                <a
                  className="w-100"
                  target={"_blank"}
                  rel="noopener noreferrer"
                  href="https://twitter.com/baitalmashura"
                >
                  <img
                    className="mx-2"
                    src={youtube}
                    alt="twitterIcon"
                  />
                </a>
              </div>
            </div>
          </div>
        </SidebarStyled>
      )}
    </Translation>
  );
};

export default Sidebar;
