import React, { useContext, useState } from "react";
import { Translation, useTranslation } from "react-i18next";
import logo1 from "../../assets/images/logo1.png";
import logo4 from "../../assets/images/logo4.png";
import logo3 from "../../assets/images/logo3.png";
import verticalSeperator from "../../assets/images/verticalSeperator.png";
import lock from "../../assets/images/lock.png";
import search from "../../assets/images/search.png";
import active from "../../assets/images/active.png";
import styles from "./header.module.scss";
import { Navbar, Nav } from "react-bootstrap";
import { Link, useLocation, useNavigate } from "react-router-dom";
import i18next from "i18next";
import greenBgStar from "../../assets/images/greenBgStar.png";
import LocaleContext from "../../LocaleContext";
import Button from "../../components/Button";
import styled from "styled-components";
import burgerIcon from "../../assets/images/Menu_sidebar.svg";
import lockfix from "../../assets/images/lock_fix.svg";
import language from "../../assets/images/language.svg";
import searchTopHead from "../../assets/images/searchTopHeader.svg";

const NavWrapper = styled.div`
  width: 100%;
  color: #84C872;
  display: flex;
  align-items: center;
  justify-content: space-between;
  position: fixed;
  top: 0;
  left: 0;
  height: 60px;
  padding: 0 10rem;
  background-color: #006548;
  z-index:555;

  @media screen and (min-device-width: 320px) and (max-device-width: 768px) {
    padding: 0 0.6rem !important;
  }
`;



const Header = ({ toggleMenu }) => {
  const { i18n } = useTranslation();
  const navigate = useNavigate();
  // const { locale } = useContext(LocaleContext);
  const [locale, setLocale] = useState(i18n.language);
  const onLanguageChange = () => {

    // window.location.reload(false);
    i18n.changeLanguage(locale.includes("en") ? "ar" : "en");

    localStorage.setItem('i18nextLng', locale.includes("en") ? "ar" : "en");
    window.location.reload(false);
  };
  const location = useLocation();
  const { pathname } = location;
  const splitLocation = pathname.split("/");
  return (
    <Translation>
      {(t) => (
        <>
          {/* <img
            className={`d-block w-50 position-absolute ${
              i18next.language === "ar" ? styles.imgAr : styles.img
            }`}
            src={greenBgStar}
            alt="First slide"
          /> */}

          {/* <div className='topFixedNavbar'>
            <div className={`${styles.headerHead} px-5`}>
              <div>
                <p>Test</p> */}
          <NavWrapper>
            <div
              className={`d-flex flex-row align-items-center ${styles.ourServDiv}`}
              onClick={() => {
                toggleMenu(true);
                console.log("Hamburger menu clicked, toggle open");
              }}
            >
              <img src={burgerIcon} alt="burger-icon"></img>
              <p className={`m-2 ${styles.ourServiceText}`}>{t("Header.ourServices")}</p>
            </div>
            <div className={`${styles.webCode}`}>
              <span>ISSN 2409-0867 ONLINE | ISSN 2410-6836 PRINT</span>
            </div>
            <div className="d-flex flex-row">
              <div className="m-3">
                <a
                  href="https://doaj.org/"
                  target={"_blank"}
                  rel="noopener noreferrer"
                >
                  <img src={lockfix} alt="lockfix"></img>
                </a>
              </div>
              <div className={`m-3 d-flex flex-row align-items-center ${styles.ourServDiv}`} onClick={onLanguageChange}>
                <img src={language} alt="language"></img>
                <div className="m-1">
                  {t("Header.lang")}
                </div>
                {/* <span></span> */}
              </div>
              <div className="mx-0 my-3">
                <Link to="/search">
                  <img src={searchTopHead} alt="search" />
                </Link>
              </div>
            </div>
          </NavWrapper>


          <div
            className={`${styles.tabView}`}
          >
            <div className={`${styles.headBackground}`}>

              <div className="container d-flex text-center align-items-center">
                <div className="col-md-4">
                  <a
                    href="https://meia-qa.org/"
                    target={"_blank"}
                    rel="noopener noreferrer"
                    className="col-md-4"
                  >
                    <img src={logo3} alt="Logo-3" className={styles.mainLogos} />
                  </a>
                </div>
                <div className="col-md-4">
                  <img
                    onClick={() => navigate("/")}
                    src={logo4}
                    alt="Logo-2"
                    className={`w-100 ${styles.mainLogos}`}
                  />
                  <div className={`mt-4 ${styles.w33}`}>
                    <p className={`${styles.header}`}>{t("Header.title")}</p>
                  </div>
                  <div className={`mt-1 ${styles.w33}`}>
                    <p className={`${styles.header}`}>{t("Header.subtitle")}</p>
                  </div>
                </div>

                <a
                  href="https://www.awqaf.gov.qa/"
                  target={"_blank"}
                  rel="noopener noreferrer"
                  className="col-md-4"
                >
                  <img src={logo1} alt="Logo-1" className={styles.mainLogos} />
                </a>
              </div>

            </div>

            {/* <div className="container d-flex justify-content-center justify-content-lg-between my-md-3 align-items-center">
              <div className={`d-flex align-items-center gap-3 ${styles.w30}`}>
                <li
                  className={
                    splitLocation[1] === "faqs"
                      ? `nav-item w-auto d-flex flex-column align-items-center mb-0 ${styles.normalLi} ${styles.link2} ${styles.activeLiThree}`
                      : `nav-item w-auto d-flex flex-column align-items-center mb-0 ${styles.normalLi} ${styles.link2}`
                  }
                >
                  <Link to="/faqs" className={styles.link2}>
                    {t("Header.FAQs")}
                  </Link>
                  <img src={active} className={styles.imgClass} alt="" />
                </li>
                <img src={verticalSeperator} className="w-auto" alt="" />
                <li
                  className={
                    splitLocation[1] === "contact-us"
                      ? `nav-item w-auto d-flex flex-column align-items-center mb-0 ${styles.normalLi} ${styles.link2} ${styles.activeLiThree}`
                      : `nav-item w-auto d-flex flex-column align-items-center mb-0 ${styles.normalLi} ${styles.link2}`
                  }
                >
                  <Link to="/contact-us" className={styles.link2}>
                    {t("Header.contactUs")}
                  </Link>
                  <img src={active} className={styles.imgClass} alt="" />
                </li>
                <img src={verticalSeperator} className="w-auto" alt="" />
                <li
                  className={
                    splitLocation[1] === "help"
                      ? `nav-item w-auto d-flex flex-column align-items-center mb-0 ${styles.normalLi} ${styles.link2} ${styles.activeLiThree}`
                      : `nav-item w-auto d-flex flex-column align-items-center mb-0 ${styles.normalLi} ${styles.link2}`
                  }
                >
                  <Link to="/help" className={styles.link2}>
                    {t("Header.help")}
                  </Link>
                  <img src={active} className={styles.imgClass} alt="" />
                </li>
              </div>
              <div className={styles.w33}>
                <p className={`${styles.header}`}>{t("Header.title")}</p>
              </div>
              <div
                className={`d-flex align-items-center gap-3 justify-content-end ${styles.w33}`}
              >
                <a href="#" className={`${styles.link2} w-auto`}>
                  {t("Header.online")}
                </a>
                <img src={verticalSeperator} className="w-auto" alt="" />
                <a href="#" className={`${styles.link2} w-auto`}>
                  {t("Header.print")}
                </a>
              </div>
            </div> */}
            <Navbar collapseOnSelect expand="sm" bg="transparent" className="p-0">
              <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className={`w-100 ${styles.navList}`}>
                  <ul
                    className={` navbar-nav me-auto mb-2 mb-lg-0 justify-content-center w-100 flex-wrap align-items-baseline ${styles.plAuto}`}
                  >
                    <li
                      className={
                        splitLocation[1] === ""
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/" className={styles.link}>
                        {t("Header.home")}
                      </Link>
                      {/* <img src={active} className={styles.imgClass} alt="" /> */}
                    </li>
                    <li
                      className={
                        splitLocation[1] === "journal"
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/journal" className={styles.link}>
                        {t("Header.journal")}
                      </Link>
                      {/* <img src={active} className={styles.imgClass} alt="" /> */}
                    </li>
                    <li
                      className={
                        splitLocation[1] === "editorial-board"
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/editorial-board" className={styles.link}>
                        {t("Header.editorialBoard")}
                      </Link>
                      {/* <img src={active} className={styles.imgClass} alt="" /> */}
                    </li>
                    <li
                      className={
                        splitLocation[1] === "advisory-board"
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/advisory-board" className={styles.link}>
                        {t("Header.advisoryBoard")}
                      </Link>
                      {/* <img src={active} className={styles.imgClass} alt="" /> */}
                    </li>
                    <li
                      className={
                        splitLocation[1] === "author"
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/author" className={styles.link}>
                        {t("Header.author")}
                      </Link>
                      {/* <img src={active} className={styles.imgClass} alt="" /> */}
                    </li>
                    <li
                      className={
                        splitLocation[1] === "publication-ethics"
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/publication-ethics" className={styles.link}>
                        {t("Header.publicationEthics")}
                      </Link>
                      {/* <img src={active} className={styles.imgClass} alt="" /> */}
                    </li>
                    <li
                      className={
                        splitLocation[1] === "index-&-database"
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/index-&-database" className={styles.link}>
                        {t("Header.index&Database")}
                      </Link>
                      {/* <img src={active} className={styles.imgClass} alt="" /> */}
                    </li>
                    <li
                      className={
                        splitLocation[1] === "archive"
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/archive" className={styles.link}>
                        {t("Header.archive")}
                      </Link>
                      {/* <img src={active} className={styles.imgClass} alt="" /> */}
                    </li>
                    {/* <li className="nav-item w-auto">
                      <Link to="/search" className={styles.link}>
                        <img src={search} alt="" />
                      </Link>
                    </li>
                    <li className="nav-item w-auto">
                      <a
                        href="https://doaj.org/"
                        target={"_blank"}
                        rel="noopener noreferrer"
                      >
                        <img src={lock} className="w-auto" alt="" />
                      </a>
                    </li> */}
                    <li className={`nav-item mx-3`}>
                      <div className={`text-center text-md-start ${styles.seemoreButton}`}>
                        <Button
                          onClick={() => navigate("/research-form")}
                          text={i18n.t("Submit")}
                          className={`${styles.contactButton}`}
                        />
                      </div>
                    </li>
                    {/* <li className={`nav-item mx-3`}>
                      <div
                        className={`justify-content-center align-items-center d-flex rounded-2 ${styles.boxGreen}`}
                      >
                        <a
                          onClick={onLanguageChange}
                          className={`fw-bold ${styles.langSwitch}`}
                        >
                          {t("Header.lang")}
                        </a>
                      </div>
                    </li> */}
                  </ul>
                </Nav>
              </Navbar.Collapse>
            </Navbar>
          </div>
          <div
            className={`container pt-2 ${styles.mobileView}`}
            style={{ zIndex: 10 }}
          >
            <Navbar collapseOnSelect expand="sm" bg="transparent">
              <div className="w-50">
                <img
                  onClick={() => navigate("/")}
                  className="d-block w-100"
                  src={logo4}
                  alt="Logo-1"
                />
              </div>
              <Navbar.Toggle aria-controls="responsive-navbar-nav" />
              <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className={`w-100 mt-3 ${styles.navList}`}>
                  <ul className="navbar-nav me-auto mb-2 mb-lg-0 justify-content-between mt-lg-3 mt-xxl-5 w-100 flex-wrap p-0">
                    <li
                      className={
                        splitLocation[1] === ""
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/" className={styles.link}>
                        {t("Header.home")}
                      </Link>
                      {/* <img src={active} className={styles.imgClass} alt="" /> */}
                    </li>
                    <li
                      className={
                        splitLocation[1] === "journal"
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/journal" className={styles.link}>
                        {t("Header.journal")}
                      </Link>
                      {/* <img src={active} className={styles.imgClass} alt="" /> */}
                    </li>
                    <li
                      className={
                        splitLocation[1] === "editorial-board"
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/editorial-board" className={styles.link}>
                        {t("Header.editorialBoard")}
                      </Link>
                      {/* <img src={active} className={styles.imgClass} alt="" /> */}
                    </li>
                    <li
                      className={
                        splitLocation[1] === "advisory-board"
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/advisory-board" className={styles.link}>
                        {t("Header.advisoryBoard")}
                      </Link>
                      {/* <img src={active} className={styles.imgClass} alt="" /> */}
                    </li>
                    <li
                      className={
                        splitLocation[1] === "author"
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/author" className={styles.link}>
                        {t("Header.author")}
                      </Link>
                      {/* <img src={active} className={styles.imgClass} alt="" /> */}
                    </li>
                    <li
                      className={
                        splitLocation[1] === "publication-ethics"
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/publication-ethics" className={styles.link}>
                        {t("Header.publicationEthics")}
                      </Link>
                      {/* <img src={active} className={styles.imgClass} alt="" /> */}
                    </li>
                    <li
                      className={
                        splitLocation[1] === "index-&-database"
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/index-&-database" className={styles.link}>
                        {t("Header.index&Database")}
                      </Link>
                      {/* <img src={active} className={styles.imgClass} alt="" /> */}
                    </li>
                    <li
                      className={
                        splitLocation[1] === "archive"
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/archive" className={styles.link}>
                        {t("Header.archive")}
                      </Link>
                      {/* <img src={active} className={styles.imgClass} alt="" /> */}
                    </li>
                    {/* <li
                      className={
                        splitLocation[1] === "faqs"
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/faqs" className={styles.link}>
                        {t("Header.FAQs")}
                      </Link>
                    </li> */}
                    {/* <li
                      className={
                        splitLocation[1] === "contact-us"
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/contact-us" className={styles.link}>
                        {t("Header.contactUs")}
                      </Link>
                    </li> */}
                    {/* <li
                      className={
                        splitLocation[1] === "help"
                          ? `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.activeLi}`
                          : `nav-item w-auto d-flex flex-column align-items-center ${styles.normalLi} ${styles.regularLi}`
                      }
                    >
                      <Link to="/help" className={styles.link}>
                        {t("Header.help")}
                      </Link>
                    </li> */}
                    <li className="nav-item w-auto d-flex justify-content-center mb-3 mb-md-0">
                      <Link to="/search" className={styles.link}>
                        <img src={search} alt="" />
                      </Link>
                    </li>
                    <li className="nav-item w-auto d-flex justify-content-center mb-3 mb-md-0">
                      <a
                        href="https://doaj.org/"
                        target={"_blank"}
                        rel="noopener noreferrer"
                      >
                        <img src={lock} className="w-auto" alt="" />
                      </a>
                    </li>
                    <li className={`nav-item justify-content-center d-flex mx-3`}>
                      <div
                        className={`justify-content-center align-items-center d-flex rounded-2 p-2 ${styles.boxGreen}`}
                      >
                        <a
                          onClick={onLanguageChange}
                          className={`fw-bold ${styles.link} ${styles.langSwitch}`}
                        >
                          {t("Header.lang")}
                        </a>
                      </div>
                    </li>
                  </ul>
                </Nav>
              </Navbar.Collapse>
            </Navbar>
          </div>
        </>
      )}
    </Translation>
  );
};

export default Header;
