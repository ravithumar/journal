import React from "react";
import styles from "./pageHeader.module.scss";
import separator from "../../assets/images/separator.png";

const PageHeader = ({ pageTitle }) => {
  return (
    <>
      <h1 className={`my-4 my-md-5 mx-auto text-center ${styles.title}`}>
        {pageTitle}
      </h1>
      <img
        className="d-block w-100 "
        style={{ height: "1rem" }}
        src={separator}
        alt="Separator"
      />
    </>
  );
};

export default PageHeader;
