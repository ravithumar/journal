import React from "react";
import Lottie from "lottie-react-web";
import animation from "../../assets/Awqaf.json";

const Loader = () => {
  return (
    <div className={`col-12 d-flex`}>
      <Lottie
        style={{ cursor: "progress" }}
        width={400}
        options={{
          animationData: animation,
        }}
      />
    </div>
  );
};

export default Loader;
