import React from "react";
import styles from "./button.module.scss";
const Button = (props) => {
  return (
    <button
      disabled={props?.disabled}
      type={props?.type ?? "button"}
      style={props?.style}
      className={`btn fs-4 border-transparent ${styles.regularButton} ${props?.className}`}
      onClick={props?.onClick}
    >
      {props?.text ?? "See More"}
    </button>
  );
};

export default Button;
