import React, { useEffect, useState } from "react";
import styles from "./footer.module.scss";
import separator from "../../assets/images/separator.png";
import footerLogo from "../../assets/images/footerLogo.png";
import arFooter1 from "../../assets/images/arFooter1.png";
import arFooter2 from "../../assets/images/newFooterLogo.svg";
import footLogo from "../../assets/images/footLogo.svg";
import locationIcon from "../../assets/images/locationIcon.png";
import msgIcon from "../../assets/images/msgIcon.png";
import twitterIcon from "../../assets/images/twitterIcon.svg";
import instagramIcon from "../../assets/images/instagramIcon.svg";
import facebookIcon from "../../assets/images/facebookIcon.svg";
import waqfaq from "../../assets/images/WAQFQA.svg";
import youtubeIcon from "../../assets/images/youtubeIcon.svg";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { Translation, useTranslation } from "react-i18next";
import i18next from "i18next";
import { useFormik } from "formik";
import { postApi } from "../../utils/ServiceManager";
import docResearch from "../../assets/images/docResearch.png";
import Button from "../../components/Button";

const validate = (values) => {
  const errors = {};
  if (!values.email) {
    errors.email =
      i18next.language === "ar" ? "البريد الإلكتروني (مطلوب" : "Email Required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email =
      i18next.language === "ar"
        ? "عنوان البريد الإلكتروني غير صالح"
        : "Invalid email address";
  }
  return errors;
};
const Footer = () => {
  const { i18n } = useTranslation();
  const navigate = useNavigate();
  const [message, setMessage] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const location = useLocation();
  const { pathname } = location;
  const splitLocation = pathname.split("/");
  useEffect(() => {
    formik.setErrors("");
  }, [location]);
  const formik = useFormik({
    initialValues: {
      email: "",
    },
    validate,
    onSubmit: (values, { resetForm }) => {
      setIsLoading(true);
      postApi(
        "subscribe",
        values,
        (res) => {
          setIsLoading(false);
          setMessage(res?.message);
          if (res?.status) {
            resetForm({ values: "" });
          }
        },
        (err) => {
          setIsLoading(false);
          setMessage(JSON.stringify(err?.message));
        }
      );
    },
  });

  return (
    <Translation>
      {(t) => (
        <footer>
          {/* <div className="container">
            {splitLocation[1] === "" ? (
              <section className="d-flex flex-column justify-content-center align-items-center bg-opacity-10 bg-white py-4 px-4 mb-5 gap-4 rounded-3">
                <h4 className="h4 text-white">{t("Footer.inputDesc")}</h4>
                <form
                  noValidate
                  className={`input-group ${styles.searchTextInput}`}
                >
                  <input
                    id="email"
                    name="email"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    type="email"
                    className={`form-control bg-transparent border-0 text-white px-2 px-md-4 ${styles.placeholder}`}
                    placeholder={t("Form.EmailAddress")}
                    aria-label="Email Address"
                    formNoValidate={true}
                  />
                  <button
                    disabled={isLoading}
                    onClick={formik.handleSubmit}
                    type={"submit"}
                    className={`btn m-1 m-md-3 ${styles.regularButton}`}
                  >
                    {isLoading ? (
                      <>
                        <span
                          className="spinner-border spinner-border-sm mx-3"
                          role="status"
                          aria-hidden="true"
                        ></span>
                        {i18next.language === "ar"
                          ? "جار التحميل..."
                          : "Loading..."}
                      </>
                    ) : (
                      <> {t("Subscribe")}</>
                    )}
                  </button>
                </form>
                {formik.errors.email && (
                  <p className="text-danger">{formik.errors.email}</p>
                )}
                {message && <p>{message}</p>}
              </section>
            ) : (
              <div
                className={`d-flex flex-column flex-md-row flex-wrap p-3 p-lg-4 p-xl-5 align-items-center gap-2 gap-md-3 gap-xl-5 mt-lg-4 mt-xl-5 ${styles.contactBox}`}
              >
                <div
                  className={`d-flex justify-content-center ${styles.imgContact}`}
                >
                  <img
                    className="d-block w-75 mb-3 mb-md-0"
                    src={docResearch}
                    alt="Research Submit"
                  />
                </div>
                <div className={`${styles.contactTitleDiv} p-3`}>
                  <h4 className={`h4 fs-2 ${styles.contactTitle}`}>
                    {i18n.t("Home.ResearchTitle")}
                  </h4>
                  <p className={`p ${styles.contactDesc}`}>
                    {i18n.t("Home.ResearchDesc")}
                  </p>
                </div>
                <Button
                  onClick={() => navigate("/research-form")}
                  text={i18n.t("SubmitHere")}
                  className={`${styles.contactButton} mt-4`}
                />
              </div>
            )}
            <img
              className={`d-block w-100 ${styles.separator}`}
              src={separator}
              alt="Separator"
            />
            <section className="d-flex flex-column align-items-center my-5 gap-3">
              <div className="d-flex flex-column align-items-center w-100">
                <div>
                  <img
                    className="d-block w-100"
                    src={footerLogo}
                    alt="footerLogo"
                  />
                </div>
                <div className="d-flex w-100 justify-content-evenly align-items-center">
                  <div className={styles.hideImages}>
                    <img
                      className="d-block w-100"
                      src={arFooter1}
                      alt="footerLogo"
                    />
                  </div>
                  <p className={`text-white text-center m-0 ${styles.title}`}>
                    {t("Header.title")}
                  </p>
                  <div className={styles.hideImages}>
                    <img
                      className="d-block w-100"
                      src={arFooter2}
                      alt="footerLogo"
                    />
                  </div>
                </div>
              </div>
              <div className="d-flex w-100 justify-content-between flex-wrap">
                <div
                  className={`col-12 col-md-6 col-lg-3 d-flex flex-column align-items-baseline ${styles.boxesFooter}`}
                >
                  <div className="d-flex flex-column">
                    <a
                      className="d-flex gap-3 justify-content-center"
                      target={"_blank"}
                      rel="noopener noreferrer"
                      href="https://www.google.com/maps/place/Doha,+Qatar/@25.2840091,51.5119967,12z/data=!4m2!3m1!1s0x3e45c534ffdce87f:0x44d9319f78cfd4b1"
                    >
                      <div>
                        <img
                          className="d-block"
                          src={locationIcon}
                          alt="footerLogo"
                        />
                      </div>
                      <p className="text-white-50">
                        <span className={styles.address}>
                          General Directorate of Endowments Ministry OF
                          Endowment and Islamic Affairs St, Doha, Qatar
                        </span>
                      </p>
                    </a>
                    <div
                      className={`d-flex flex-column flex-md-row ${styles.postOffice}`}
                    >
                      <p className="text-white-50">P.O.Box xxxxxxxxxxxx</p>
                      <a
                        className="d-flex gap-3 justify-content-md-center"
                        href="mailto:xxxxxxxxxxxx"
                      >
                        <div>
                          <img
                            className="d-block w-100"
                            src={msgIcon}
                            alt="footerLogo"
                          />
                        </div>
                        <p className="text-white-50">xxxxxxxxxxxx</p>
                      </a>
                    </div>
                  </div>
                </div>
                <div
                  className={`col-12 col-md-6 col-lg-3 d-flex flex-column align-items-start gap-3 ${styles.boxesFooter}`}
                >
                  <h6 className="h5 mb-3 text-white">{t("Footer.Links")}</h6>
                  <div className="d-flex gap-3">
                    <a
                      className="w-100"
                      target={"_blank"}
                      rel="noopener noreferrer"
                      href="https://www.facebook.com/mashurajournal.qa/"
                    >
                      <img
                        className="d-block w-100"
                        src={facebookIcon}
                        alt="facebookIcon"
                      />
                    </a>
                    <a
                      className="w-100"
                      target={"_blank"}
                      rel="noopener noreferrer"
                      href="https://www.instagram.com/bait_almashura/"
                    >
                      <img
                        className="d-block w-100"
                        src={instagramIcon}
                        alt="instagramIcon"
                      />
                    </a>
                    <a
                      className="w-100"
                      target={"_blank"}
                      rel="noopener noreferrer"
                      href="https://twitter.com/baitalmashura"
                    >
                      <img
                        className="d-block w-100"
                        src={twitterIcon}
                        alt="twitterIcon"
                      />
                    </a>
                  </div>
                </div>
                <div
                  className={`col-12 col-md-6 col-lg-3 d-flex flex-column align-items-start justify-content-between ${styles.boxesFooter}`}
                >
                  <h6 className="h5 mb-3 text-white">{t("Footer.Company")}</h6>
                  <Link to="/author">
                    <h6 className="h5 mb-3 text-white">{t("Author.header")}</h6>
                  </Link>
                  <Link to="/journal">
                    <h6 className="h5 mb-3 text-white">{t("About.header")}</h6>
                  </Link>
                </div>
                <div
                  className={`col-12 col-md-6 col-lg-3 d-flex flex-column align-items-start justify-content-between ${styles.boxesFooter}`}
                >
                  <Link to="/">
                    <h6 className="h5 mb-3 text-white">{t("Header.home")}</h6>
                  </Link>
                  <Link to="/archive">
                    <h6 className="h5 mb-3 text-white">
                      {t("Header.archive")}
                    </h6>
                  </Link>
                  <Link to="/contact-us">
                    <h6 className="h5 mb-3 text-white">
                      {t("Header.contactUs")}
                    </h6>
                  </Link>
                </div>
              </div>
            </section>
          </div> */}

          <div class={`${styles.subscribeSection}`}>
            <div className="container">
              {splitLocation[1] === "" ? (
                <section className="d-flex flex-column justify-content-center align-items-center bg-opacity-10 bg-white p-5 gap-4 rounded-3">
                  {/* <h4 className={`h4" ${styles.inputHeadText}`}>{t("Footer.inputDesc")}</h4> */} 
                  <h4 className={`h4" ${styles.inputHeadText}`}><span>{t("Footer.Wantthe")}</span> {t("Footer.latestUpdate")} <span>{t("Footer.dailynewsletter")}</span></h4>
                  <form
                    noValidate
                    className={`input-group ${styles.searchTextInput}`}
                  >
                    <input
                      id="email"
                      name="email"
                      value={formik.values.email}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      type="email"
                      className={`form-control bg-transparent border-0 text-white px-2 px-md-4 ${styles.placeholder}`}
                      placeholder={t("Form.EmailAddress")}
                      aria-label="Email Address"
                      formNoValidate={true}
                    />
                    <button
                      disabled={isLoading}
                      onClick={formik.handleSubmit}
                      type={"submit"}
                      className={`btn m-1 m-md-3 ${styles.regularButton}`}
                    >
                      {isLoading ? (
                        <>
                          <span
                            className="spinner-border spinner-border-sm mx-3"
                            role="status"
                            aria-hidden="true"
                          ></span>
                          {i18next.language === "ar"
                            ? "جار التحميل..."
                            : "Loading..."}
                        </>
                      ) : (
                        <> {t("Subscribe")}</>
                      )}
                    </button>
                  </form>
                  {formik.errors.email && (
                    <p className="text-danger">{formik.errors.email}</p>
                  )}
                  {message && <p>{message}</p>}
                </section>
              ) : (
                <div
                  className={`d-flex flex-column flex-md-row flex-wrap p-3 p-lg-4 p-xl-5 align-items-center gap-2 gap-md-3 gap-xl-5 mt-lg-4 mt-xl-5 ${styles.contactBox}`}
                >
                  <div
                    className={`d-flex justify-content-center ${styles.imgContact}`}
                  >
                    <img
                      className="d-block w-75 mb-3 mb-md-0"
                      src={docResearch}
                      alt="Research Submit"
                    />
                  </div>
                  <div className={`${styles.contactTitleDiv} p-3`}>
                    <h4 className={`h4 fs-2 ${styles.contactTitle}`}>
                      {i18n.t("Home.ResearchTitle")}
                    </h4>
                    <p className={`p ${styles.contactDesc}`}>
                      {i18n.t("Home.ResearchDesc")}
                    </p>
                  </div>
                  <Button
                    onClick={() => navigate("/research-form")}
                    text={i18n.t("SubmitHere")}
                    className={`${styles.contactButton} mt-4`}
                  />
                </div>
              )}
            </div>
          </div>


          <div class={`${styles.footerBackground}`}>
            <div className="container py-5">
              <div className="row">
                <div className="col-md-4 d-flex justify-content-center align-items-center">
                  <div
                    className={` ${i18next.language === "ar"
                      ? styles.boxesFooterAr
                      : styles.boxesFooter
                      }`}
                  >
                    <h6 className="mb-3">{t("Footer.Company")}</h6>
                    <Link to="/author">
                      <h6 className="mb-3 ">{t("Author.header")}</h6>
                    </Link>
                    <Link to="/journal">
                      <h6 className="mb-3 ">{t("About.header")}</h6>
                    </Link>
                    <Link to="/contact-us">
                      <h6 className="mb-3 ">
                      {t("Header.help")}
                      </h6>
                    </Link>
                  </div>
                </div>
                <div className="col-md-4 d-flex justify-content-center align-items-center">
                  <div
                    className={` ${i18next.language === "ar"
                      ? styles.boxesFooterAr
                      : styles.boxesFooter
                      }`}
                  >
                    <Link to="/">
                      <h6 className="mb-3 ">{t("Header.home")}</h6>
                    </Link>
                    <Link to="/archive">
                      <h6 className="mb-3 ">
                        {t("Header.archive")}
                      </h6>
                    </Link>
                    <Link to="/contact-us">
                      <h6 className="mb-3 ">
                        {t("Header.contactUs")}
                      </h6>
                    </Link>
                    <Link to="/faqs">
                      <h6 className="mb-3 ">
                        {t("Header.faqs")}
                      </h6>
                    </Link>
                  </div>
                </div>
                <div className="col-md-4 d-flex flex-column justify-content-center align-items-center">

                  <div className="d-flex">
                    <div>
                      <img
                        className=""
                        src={waqfaq}
                        alt="footerLogo"
                      />
                    </div>
                    <div className="mx-3">
                      <a
                        className=""
                        target={"_blank"}
                        rel="noopener noreferrer"
                        href="https://www.facebook.com/mashurajournal.qa/"
                      >
                        <img
                          className="mx-1"
                          src={facebookIcon}
                          alt="facebookIcon"
                        />
                      </a>
                      <a
                        className="w-100"
                        target={"_blank"}
                        rel="noopener noreferrer"
                        href="https://www.instagram.com/bait_almashura/"
                      >
                        <img
                          className="mx-1"
                          src={instagramIcon}
                          alt="instagramIcon"
                        />
                      </a>
                      <a
                        className="w-100"
                        target={"_blank"}
                        rel="noopener noreferrer"
                        href="https://twitter.com/baitalmashura"
                      >
                        <img
                          className="mx-1"
                          src={twitterIcon}
                          alt="twitterIcon"
                        />
                      </a>
                      <a
                        className="w-100"
                        target={"_blank"}
                        rel="noopener noreferrer"
                        href="https://twitter.com/baitalmashura"
                      >
                        <img
                          className="mx-1"
                          src={youtubeIcon}
                          alt="twitterIcon"
                        />
                      </a>
                    </div>
                  </div>

                  <div className="d-flex mt-4">
                    <div>
                      <img
                        className=""
                        src={footLogo}
                        alt="footerLogo"
                      />
                    </div>
                    <div className="mx-3 d-flex align-items-end">
                      <img
                        className=""
                        src={arFooter2}
                        alt="footerLogo"
                      />
                    </div>
                  </div>
                </div>
              </div>


              {/* <div className="d-flex w-100 justify-content-between flex-wrap">
                <div>
                  <img
                    className="d-block w-100"
                    src={arFooter2}
                    alt="footerLogo"
                  />
                </div>
                <div
                  className={`col-12 col-md-6 col-lg-3 d-flex flex-column align-items-start justify-content-between ${i18next.language === "ar"
                    ? styles.boxesFooterAr
                    : styles.boxesFooter
                    }`}
                >
                  <h6 className="mb-3">{t("Footer.Company")}</h6>
                  <Link to="/author">
                    <h6 className="mb-3 ">{t("Author.header")}</h6>
                  </Link>
                  <Link to="/journal">
                    <h6 className="mb-3 ">{t("About.header")}</h6>
                  </Link>
                  <Link to="/journal">
                    <h6 className="mb-3 ">Help</h6>
                  </Link>
                </div>
                <div
                  className={`col-12 col-md-6 col-lg-3 d-flex flex-column align-items-start justify-content-between ${i18next.language === "ar"
                    ? styles.boxesFooterAr
                    : styles.boxesFooter
                    }`}
                >
                  <Link to="/">
                    <h6 className="mb-3 ">{t("Header.home")}</h6>
                  </Link>
                  <Link to="/archive">
                    <h6 className="mb-3 ">
                      {t("Header.archive")}
                    </h6>
                  </Link>
                  <Link to="/contact-us">
                    <h6 className="mb-3 ">
                      {t("Header.contactUs")}
                    </h6>
                  </Link>
                  <Link to="/contact-us">
                    <h6 className="mb-3 ">
                      FAQs
                    </h6>
                  </Link>
                </div>
              </div> */}
            </div>
          </div>

          <div className={`d-flex justify-content-center ${styles.bgGreen}`}>
            <p className="my-1">{t("Footer.Copyright")}</p>
          </div>
        </footer>
      )}
    </Translation>
  );
};

export default Footer;
